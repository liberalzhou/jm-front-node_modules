import Layout from '@/layout'
import menuLayout from '@/layout/menu'
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/publicHome',
    children: [{
      path: 'publicHome',
      name: 'publicHome',
      component: () => import('@/views/mainSystem/publicHome/index'),
      meta: { system: ['main'], title: '进入页', icon: 'iconfont icon-shouye' },
      hidden: true
    }]
  },
  {
    path: '/profile',
    component: Layout,
    children: [{
      path: 'index',
      name: 'profile',
      component: () => import('@/views/mainSystem/profile/index'),
      meta: { title: '个人信息', icon: 'iconfont icon-yonghuguanli' },
      hidden: true
    }]
  },
  {
    path: '/changePassword',
    component: Layout,
    children: [{
      path: 'index',
      name: 'changePassword',
      component: () => import('@/views/mainSystem/changePassword/index'),
      meta: { title: '修改密码', icon: 'iconfont icon-gaimima' },
      hidden: true
    }]
  }
]
/**
 * meta.system 代表路由所属系统，路由可能会被多个系统共用
*/
export const asyncRoutes = [
// 主系统mainSystem---------------------------  
  {
    path: '/accountSettings',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:getUserPageList' },
    children: [{
      path: 'index',
      name: 'accountSettings',
      component: () => import('@/views/mainSystem/accountSettings/index'),
      meta: { system: ['main'], menuKey: 'sys:getUserPageList', SYSTEM_FLAG: '1000', title: '账号设置', icon: 'iconfont icon-zhanghaoshezhi' }
    }]
  },
  {
    path: '/userControl',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:getAccountPageList' },
    children: [{
      path: 'index',
      name: 'userControl',
      component: () => import('@/views/mainSystem/userControl/index'),
      meta: { system: ['main'], menuKey: 'sys:getAccountPageList', SYSTEM_FLAG: '1000', title: '用户管理', icon: 'iconfont icon-yonghuguanli' }
    }]
  },
  {
    path: '/messageTemplate',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:template:listPage' },
    children: [{
      path: 'index',
      name: 'messageTemplate',
      component: () => import('@/views/mainSystem/messageTemplate/index'),
      meta: { system: ['main'], menuKey: 'sys:template:listPage', SYSTEM_FLAG: '1000', title: '通知模板', icon: 'iconfont icon-mobanguanli' }
    }]
  },
  {
    path: '/rolePermission',
    component: Layout,
    redirect: '/rolePermission/index',
    meta: { system: ['main'], menuKey: 'sys:getRoleList' },
    children: [{
      path: 'index',
      name: 'rolePermission',
      component: () => import('@/views/mainSystem/rolePermission/index'),
      meta: { system: ['main'], menuKey: 'sys:getRoleList', SYSTEM_FLAG: '1000', title: '角色权限', icon: 'iconfont icon-jiaosequanxian' }
    },
    {
      path: 'addRole',
      name: 'AddRole',
      component: () => import('@/views/mainSystem/rolePermission/addRole'),
      meta: { system: ['main'], menuKey: 'sys:addRole', SYSTEM_FLAG: '1000', title: '添加角色' },
      hidden: true
    },
    {
      path: 'editRole',
      name: 'EditRole',
      component: () => import('@/views/mainSystem/rolePermission/editRole'),
      meta: { system: ['main'], menuKey: 'sys:editRole', SYSTEM_FLAG: '1000', title: '编辑角色', activeMenu: '/rolePermission/index' },
      hidden: true
    }
    ]
  },
  {
    path: '/systemSettings',
    component: Layout,
    meta: { system: ['main'], menuKey: 'sys:getSysInfo' },
    children: [{
      path: 'index',
      name: 'systemSettings',
      component: () => import('@/views/mainSystem/systemSettings/index'),
      meta: { system: ['main'], menuKey: 'sys:getSysInfo', SYSTEM_FLAG: '1000', title: '系统设置', icon: 'iconfont icon-zhanghaoshezhi' }
    }]
  },

  // 咨询公司----------------------------------

  // 项目管理
  {
    path: '/project',
    component: Layout,
    redirect: '/project/indexNew',
    alwaysShow: true,
    meta: { system: ['zixun'], menuKey: 'project:engineeringIndex', title: '项目管理', icon: 'iconfont icon-xiangmuguanli' },
    children: [

      {
        path: 'indexNew',
        name: 'newProject',
        component: () => import('@/views/project/bigProject/indexNew'),
        meta: { system: ['zixun'], menuKey: 'project:engineering:getProjectPageList', title: '项目库' }
      },
      {
        path: 'importProject',
        name: 'ImportProject',
        component: () => import('@/views/project/importProject'),
        meta: { system: ['zixun'], menuKey: 'project:baseIndex', title: '导入项目' }
      },
      {
        path: 'index',
        name: 'Projects',
        component: () => import('@/views/project/index'),
        meta: { system: ['zixun'], menuKey: 'project:detail:getProjectPageList', title: '单项工程库' }
      },
      {
        path: 'compareBigProject',
        name: 'CompareBigProject',
        component: () => import('@/views/project/bigProject/compareProject'),
        meta: { system: ['zixun'], title: '项目对比详情', activeMenu: '/project/indexNew' },
        hidden: true
      },
      {
        path: 'compareBigStage',
        name: 'compareBigStage',
        component: () => import('@/views/project/bigProject/compareStage'),
        meta: { system: ['zixun'], title: '阶段对比详情', activeMenu: '/project/indexNew' },
        hidden: true
      },
      {
        path: 'projectBigComparison',
        name: 'projectBigComparison',
        component: () => import('@/views/project/bigProject/projectComparison'),
        meta: { system: ['zixun'], title: '项目对比详情', activeMenu: '/project/indexNew' },
        hidden: true
      },
      {
        path: 'wholeBigComparison',
        name: 'wholeBigComparison',
        component: () => import('@/views/project/bigProject/wholeComparison'),
        meta: { system: ['zixun'], title: '阶段对比详情', activeMenu: '/project/indexNew' },
        hidden: true
      },
      {
        path: 'compareProject',
        name: 'CompareProject',
        component: () => import('@/views/project/compareProject'),
        // component: () => import('@/views/project/projectComparison'),
        meta: { system: ['zixun'], title: '项目对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'compareStage',
        name: 'compareStage',
        component: () => import('@/views/project/compareStage'),
        // component: () => import('@/views/project/wholeComparison'),
        meta: { system: ['zixun'], title: '阶段对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'projectComparison',
        name: 'projectComparison',
        component: () => import('@/views/project/projectComparison'),
        meta: { system: ['zixun'], title: '项目对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'wholeComparison',
        name: 'wholeComparison',
        component: () => import('@/views/project/wholeComparison'),
        meta: { system: ['zixun'], title: '阶段对比详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'projectVisible',
        name: 'ProjectVisible',
        component: () => import('@/views/project/projectVisible'),
        meta: { system: ['zixun'], menuKey: 'project:detail:getProjectEngDetailPriceView', title: '可视化', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'projectVisibleNew',
        name: 'projectVisibleNew',
        component: () => import('@/views/project/bigProject/projectVisible'),
        meta: { system: ['zixun'], menuKey: 'project:engineering:projectVisualization', title: '可视化', activeMenu: '/project/indexNew' },
        hidden: true
      },
      {
        path: 'detail',
        name: 'Detail',
        component: () => import('@/views/project/detail'),
        meta: { system: ['zixun'], menuKey: 'project:detail:getProjectBaseInfo', title: '项目详情', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'detailNew',
        name: 'DetailNew',
        component: () => import('@/views/project/bigProject/detail'),
        meta: { system: ['zixun'], menuKey: 'project:engineering:getProjectBaseInfo', title: '项目详情', activeMenu: '/project/indexNew' },
        hidden: true
      },
      {
        path: 'report',
        name: 'Peport',
        component: () => import('@/views/project/testing/index'),
        meta: { system: ['zixun'], menuKey: 'project:base:medicalReport', title: '体检报告', activeMenu: '/project/index' },
        hidden: true
      },
      {
        path: 'testing',
        name: 'Testing',
        component: () => import('@/views/project/testing/doing'),
        meta: { system: ['zixun'], menuKey: 'project:base:medicalReport', title: '项目体检', activeMenu: '/project/index' },
        hidden: true
      }
    ]
  },
  // 指标管理
  {
    path: '/target',
    component: Layout,
    redirect: '/target/index',
    alwaysShow: true,
    meta: { system: ['zixun'], menuKey: 'indicator:baseIndex', title: '指标管理', icon: 'iconfont icon-zhibiaoguanli' },
    children: [
      {
        path: 'index',
        name: 'Targets',
        component: () => import('@/views/target/index'),
        meta: { system: ['zixun'], menuKey: 'indicator:base:getMetricsList', title: '指标库' }
      },
      {
        path: 'synthesize',
        name: 'synthesize',
        component: () => import('@/views/target/synthesizeIndex'),
        meta: { system: ['zixun'], menuKey: 'indicator:plural:getPluralMetricsList', title: '综合指标库' }
      },
      {
        path: 'synthesizeExamine',
        name: 'SynthesizeExamine',
        component: () => import('@/views/target/synthesizeExamine'),
        meta: { system: ['zixun'], menuKey: 'indicator:base:getPluralMetrics', title: '综合指标审核' }
      },
      {
        path: 'detail',
        name: 'TargetDetail',
        component: () => import('@/views/target/detail/index'),
        meta: { system: ['zixun'], title: '指标库详情', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'calculate',
        name: 'Calculate',
        component: () => import('@/views/target/indexCalculation'),
        meta: { system: ['zixun'], title: '指标计算', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'analysis',
        name: 'Analysis',
        component: () => import('@/views/target/indexAnalysis'),
        meta: { system: ['zixun'], title: '指标分析', activeMenu: '/target/index' },
        hidden: true
      },
      {
        path: 'synthesizeDetail',
        name: 'SynthesizeDetail',
        component: () => import('@/views/target/detail/synthesizeCalculate'),
        meta: { system: ['zixun'], title: '综合指标库详情', activeMenu: '/target/synthesize' },
        hidden: true
      }
    ]
  },

  // 指标应用
  {
    path: '/targetApplication',
    component: Layout,
    redirect: '/targetApplication/TargetCost/bigCalcula',
    alwaysShow: true,
    // meta: { title: '新项目智能应用', icon: 'iconfont icon-zhibiaoyingyong' },
    meta: { system: ['zixun'], menuKey: 'indicator:costcalculate:Index', title: '指标应用', icon: 'iconfont icon-zhibiaoyingyong' },
    children: [
      {
        path: 'TargetCost',
        component: menuLayout,
        redirect: '/targetApplication/TargetCost/bigCalcula',
        meta: { system: ['zixun'], menuKey: 'indicator:costcalculate:listPageIndex', title: '快速造价' },
        children: [
          {
            path: 'bigCalcula',
            name: 'bigCalcula',
            component: () => import('@/views/targetApplication/bigCalcula'),
            meta: { system: ['zixun'], menuKey: 'indicator:costcalculate:project:listPage', title: '项目测算' }
          },
          {
            path: 'Calcula',
            name: 'Calcula',
            component: () => import('@/views/targetApplication/index'),
            meta: { system: ['zixun'], menuKey: 'indicator:costcalculate:listPage', title: '单项工程测算' }
          }
        ]
      },
      {
        path: 'newCalcula',
        name: 'NewCalcula',
        component: () => import('@/views/targetApplication/newCalculation'),
        meta: { system: ['zixun'], title: '快速造价', activeMenu: '/TargetCost/Calcula' },
        hidden: true
      },
      {
        path: 'calculaDetail',
        name: 'CalculaDetail',
        component: () => import('@/views/targetApplication/calculaDetail'),
        meta: { system: ['zixun'], title: '测算方案详情', activeMenu: '/TargetCost/Calcula' },
        hidden: true
      },
      {
        path: 'bigNewCalculation',
        name: 'bigNewCalculation',
        component: () => import('@/views/targetApplication/bigNewCalculation'),
        meta: { system: ['zixun'], title: '快速造价', activeMenu: '/TargetCost/bigCalcula' },
        hidden: true
      },
      {
        path: 'bigCalculaDetail',
        name: 'bigCalculaDetail',
        component: () => import('@/views/targetApplication/bigCalculaDetail'),
        meta: { system: ['zixun'], title: '测算方案详情', activeMenu: '/TargetCost/bigCalcula' },
        hidden: true
      },
      {
        path: 'engineerConversion',
        name: 'EngineerConversion',
        component: () => import('@/views/targetApplication/engineerConversion'),
        meta: { system: ['zixun'], menuKey: 'matrix:project:listMatrixMatPage', title: '同类工程换算' }
        // hidden: true,  // czhui
      },
      {
        path: 'conversionHistory',
        name: 'ConversionHistory',
        component: () => import('@/views/targetApplication/conversionHistory'),
        meta: { system: ['zixun'], title: '同类工程换算历史', activeMenu: '/targetApplication/engineerConversion' },
        hidden: true
      },
      {
        path: 'editConversion',
        name: 'EditConversion',
        component: () => import('@/views/targetApplication/editConversion'),
        meta: { system: ['zixun'], title: '工程换算详情', activeMenu: '/targetApplication/engineerConversion' },
        hidden: true
      },
      {
        path: 'targetReal',
        name: 'TargetReal',
        component: () => import('@/views/targetApplication/targetRealtime'),
        meta: { system: ['zixun'], menuKey: 'indicator:consult:listPage', title: '实时指标' }
      },
      {
        path: 'targetTest',
        name: 'TargetTest',
        component: () => import('@/views/targetApplication/targetTesting'),
        meta: { system: ['zixun'], menuKey: 'project:indicator:createOrUpdateDetectProject', title: '智能检测' }
      },
      {
        path: 'projectBuild',
        name: 'ProjectBuild',
        component: () => import('@/views/targetApplication/projectBuild'),
        meta: { system: ['zixun'], menuKey: 'matrix:build:getUnitProjectType', title: '项目自由构建' }
      },
      {
        path: 'projectBuildHistory',
        name: 'projectBuildHistory',
        component: () => import('@/views/targetApplication/projectBuildHistory'),
        meta: { system: ['zixun'], menuKey: 'matrix:build:history:getMatrixBuildHistoryPage', title: '构建历史' }
      }
    ]
  },

  // 设置中心
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/projectSet/costStage',
    name: 'setting',
    alwaysShow: true,
    meta: { system: ['zixun'], menuKey: 'configIndex', title: '设置中心', icon: 'iconfont icon-zhanghaoshezhi' },
    children: [
      {
        path: 'projectSet',
        component: menuLayout,
        redirect: '/setting/projectSet/costStage',
        meta: { system: ['zixun'], menuKey: 'project:config', title: '项目库设置' },
        children: [
          {
            path: 'costStage',
            name: 'CostStage',
            component: () => import('@/views/projectSet/costStage'),
            meta: { system: ['zixun'], title: '造价阶段标准配置' }
          },
          {
            path: 'unitProject',
            name: 'UnitProject',
            component: () => import('@/views/projectSet/unitProject'),
            meta: { system: ['zixun'], title: '单位工程标准表配置' }
          },
          {
            path: 'branchProject',
            name: 'BranchProject',
            component: () => import('@/views/projectSet/branchProject'),
            meta: { system: ['zixun'], title: '分部工程标准表配置', activeMenu: '/setting/projectSet/unitProject' },
            hidden: true
          },
          {
            path: 'skillFeature',
            name: 'SkillFeature',
            component: () => import('@/views/projectSet/skillFeature'),
            meta: { system: ['zixun'], title: '技术特征配置' }
          },
          {
            path: 'quotaStandard',
            name: 'QuotaStandard',
            component: () => import('@/views/projectSet/quotaStandard'),
            meta: { system: ['zixun'], title: '造价定额规范配置' }
          }
        ]
      },
      {
        path: 'targetSet',
        name: 'targetSet',
        redirect: '/setting/targetSet/earlyWarn',
        component: menuLayout,
        meta: { system: ['zixun'], title: '指标设置' },
        children: [
          {
            path: 'earlyWarn',
            name: 'EarlyWarn',
            component: () => import('@/views/targetSet/earlyWarning'),
            meta: { system: ['zixun'], menuKey: 'indicator:econwarn:listPage', title: '预警设置' }
          },
          {
            path: 'addWarn',
            name: 'AddWarn',
            component: () => import('@/views/targetSet/operates/addEarlyWarn'),
            meta: { system: ['zixun'], title: '配置预警指标', activeMenu: '/setting/targetSet/earlyWarn' },
            hidden: true
          },
          {
            path: 'economicTarget',
            name: 'EconomicTarget',
            component: () => import('@/views/targetSet/economicTarget'),
            meta: { system: ['zixun'], title: '实时单方造价配置' }
          },
          {
            path: 'addEconomic',
            name: 'AddEconomic',
            component: () => import('@/views/targetSet/operates/addEconomicTarget'),
            meta: { system: ['zixun'], title: '配置实时单方造价', activeMenu: '/setting/targetSet/economicTarget' },
            hidden: true
          },
          {
            path: 'technologyTarget',
            name: 'TechnologyTarget',
            component: () => import('@/views/targetSet/technologyTarget'),
            meta: { system: ['zixun'], title: '实时技术指标配置' }
          },
          {
            path: 'addTechnology',
            name: 'AddTechnology',
            component: () => import('@/views/targetSet/operates/addTechnologyTarget'),
            meta: { system: ['zixun'], title: '配置实时技术指标', activeMenu: '/setting/targetSet/technologyTarget' },
            hidden: true
          }
          // {
          //   path: 'economicConfigure',
          //   name: 'EconomicConfigure',
          //   component: () => import('@/views/targetSet/economicConfigure'),
          //   meta: { title: '单方造价生成配置' }
          // },
          // {
          //   path: 'technologyConfigure',
          //   name: 'TechnologyConfigure',
          //   component: () => import('@/views/targetSet/technologyConfigure'),
          //   meta: { title: '技术指标生成配置' }
          // },
        ]
      }

    ]
  },

  // 采算编发------------------------------------------------
  {
    path: '/cs/dashboard',
    redirect: '/cs/dashboard/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'mpcsys:sett:getHomePage' },
    children: [{
      path: 'index',
      name: 'cs/Dashboard',
      meta: { system: ['caisuan'], menuKey: 'mpcsys:sett:getHomePage', title: '首页', icon: 'iconfont icon-shouye' },
      component: () => import('@/views/cs/dashboard/index')
    }]
  },
  {
    path: '/cs/modeManagement',
    redirect: '/cs/modeManagement/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'mpccollect:collectTemplateIndex', title: '模板管理', icon: 'iconfont icon-mobanguanli' },
    children: [{
      path: 'index',
      name: 'modeManagement',
      meta: { system: ['caisuan'], menuKey: 'mpccollect:collectTemplateIndex', title: '模板管理', breadcrumb: false, activeMenu: '/cs/modeManagement/index' },
      component: () => import('@/views/cs/modeManagement/index')
    },
    {
      path: 'modeDetail',
      name: 'modeDetail',
      meta: { system: ['caisuan'], menuKey: 'mpccollect:collectTemplateDetail:getCollectTemplateDetailListPage', title: '模板库详情', activeMenu: '/cs/modeManagement/index' },
      component: () => import('@/views/cs/modeManagement/modeDetail'),
      hidden: true
    }]
  },
  {
    path: '/cs/dataCollection',
    redirect: '/cs/dataCollection/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'complexIndex' },
    children: [{
      path: 'index',
      name: 'dataCollection',
      meta: { system: ['caisuan'], menuKey: 'complexIndex', title: '数据采集提取', icon: 'iconfont icon-shujucaijitiqu' },
      component: () => import('@/views/cs/dataCollection/index')
    }]
  },
  {
    path: '/cs/EvaluationPrice',
    redirect: '/cs/EvaluationPrice/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'complex:compReviewIndex', title: '测算价评审' },
    children: [{
      path: 'index',
      name: 'EvaluationPrice',
      meta: { system: ['caisuan'], menuKey: 'complex:compReviewIndex', title: '测算价评审', icon: 'iconfont icon-cesuanjiapingshen', breadcrumb: false, activeMenu: '/cs/EvaluationPrice/index' },
      component: () => import('@/views/cs/EvaluationPrice/index')
      // hidden: true
    },
    {
      path: 'EvaluationPriceDetail',
      name: 'EvaluationPriceDetail',
      meta: { system: ['caisuan'], menuKey: 'complex:getComplexPrices', title: '评审详情', activeMenu: '/cs/EvaluationPrice/index' },
      component: () => import('@/views/cs/EvaluationPrice/EvaluationPriceDetail'),
      hidden: true
    }
    ]
  },
  {
    path: '/cs/Information',
    redirect: '/cs/Information/index',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'mpccollect:messagePriceIndex', title: '信息价' },
    children: [{
      path: 'index',
      name: 'Information',
      meta: { system: ['caisuan'], menuKey: 'mpccollect:messagePriceIndex', title: '信息价', icon: 'iconfont icon-xinxijia', breadcrumb: false, activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/index')
      // hidden: true
    },
    {
      path: 'infoPriceDetail',
      name: 'infoPriceDetail',
      meta: { system: ['caisuan'], menuKey: 'mpccollect:messagePrice:getMessagePriceDetailListPage', title: '信息价详情', activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/infoPriceDetail'),
      hidden: true
    },
    {
      path: 'historyPriceDetail',
      name: 'historyPriceDetail',
      meta: { system: ['caisuan'], menuKey: 'mpccollect:messagePriceHistory:getMessagePriceDetailHistoryListPage', title: '历史库详情', activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/historyPriceDetail'),
      hidden: true
    },
    {
      path: 'recycleBinDetail',
      name: 'recycleBinDetail',
      meta: { system: ['caisuan'], title: '回收站信息价详情', activeMenu: '/cs/Information/index' },
      component: () => import('@/views/cs/Information/recycleBinDetail'),
      hidden: true
    }
    ]
  },

  {
    path: '/cs/releaseManagement',
    component: Layout,
    redirect: '/cs/releaseManagement/periodManager',
    // alwaysShow: true,
    meta: { system: ['caisuan'], menuKey: 'mpccollect:journal:getJournalListPage', title: '发布管理' },
    children: [
      {
        path: 'periodManager',
        name: 'periodManager',
        // meta: { menuKey: 'to_mpc_home', title: '期刊管理' },
        meta: { system: ['caisuan'], menuKey: 'mpccollect:journal:getJournalListPage', title: '发布管理', icon: 'iconfont icon-fabuguanli', breadcrumb: false, activeMenu: '/cs/releaseManagement/periodManager' },
        component: () => import('@/views/cs/releaseManagement/periodManager')
        // hidden: true
      },
      {
        path: 'PeriodicalDetail',
        name: 'PeriodicalDetail',
        meta: { system: ['caisuan'], title: '期刊详情', activeMenu: '/cs/releaseManagement/periodManager' },
        component: () => import('@/views/cs/releaseManagement/periodicalDetail'),
        hidden: true
      },
      {
        path: 'issueRecordDetail',
        name: 'issueRecordDetail',
        meta: { system: ['caisuan'], title: '发布记录信息价详情', activeMenu: '/cs/releaseManagement/periodManager' },
        component: () => import('@/views/cs/releaseManagement/issueRecordDetail'),
        hidden: true
      }
    ]
  },
  {
    path: '/cs/materialManage',
    component: Layout,
    redirect: '/cs/materialManage',
    meta: { system: ['caisuan'], menuKey: 'mpccollect:collectMessagerInfo:getCollectMessagerInfoDetailPage' },
    children: [
      {
        path: 'UserManage',
        name: 'UserManage',
        meta: { system: ['caisuan'], menuKey: 'mpccollect:collectMessagerInfo:getCollectMessagerInfoDetailPage', title: '采集员管理', icon: 'iconfont icon-caijiyuanguanli' },
        component: () => import('@/views/cs/materialManage/userManage')
      }
    ]
  },
  {
    path: '/cs/appraiseExamine',
    redirect: '/cs/appraiseExamine/materialCount',
    component: Layout,
    meta: { system: ['caisuan'], menuKey: 'mpccollect:evaluate:queryMaterialCount', title: '评价考核', icon: 'iconfont icon-mobanguanli' },
    children: [
      {
        path: 'materialCount',
        name: 'materialCount',
        meta: { system: ['caisuan'], menuKey: 'mpccollect:evaluate:queryMaterialCount', title: '评价考核', breadcrumb: false, activeMenu: '/cs/appraiseExamine/materialCount' },
        component: () => import('@/views/cs/appraiseExamine/materialCount')
      },
      {
        path: 'materialCountDetail',
        component: menuLayout,
        redirect: '/cs/appraiseExamine/materialCountDetail/index',
        meta: { system: ['caisuan'], menuKey: 'mpccollect:evaluate:queryMaterialDetailCount', title: '评价考核详情', activeMenu: '/cs/appraiseExamine/materialCount' },
        hidden: true,
        children: [
          {
            path: 'index',
            name: 'materialCountDetail',
            meta: { system: ['caisuan'], menuKey: 'mpccollect:evaluate:queryMaterialDetailCount', title: '评价考核详情', breadcrumb: false, activeMenu: '/cs/appraiseExamine/materialCount' },
            component: () => import('@/views/cs/appraiseExamine/materialCountDetail')
          },
          {
            path: 'materialDetail',
            name: 'materialDetail',
            meta: { system: ['caisuan'], title: '材价详情', activeMenu: '/cs/appraiseExamine/materialCount' },
            component: () => import('@/views/cs/appraiseExamine/materialDetail')
          }]
      }]
  },
  {
    path: '/cs/setting',
    // name: 'HomeSet',
    component: Layout,
    alwaysShow: true,
    redirect: '/cs/setting/modeSet',
    meta: { system: ['caisuan'], menuKey: 'config:config:index', title: '参数设置', icon: 'iconfont icon-zhanghaoshezhi' },
    children: [{
      path: 'homeSet',
      name: 'HomeSet',
      meta: { system: ['caisuan'], menuKey: 'mpccollect:collectTemplateDetail:getMainMaterialList', title: '首页统计设置' },
      component: () => import('@/views/cs/setting/homeSet')
    },
    {
      path: 'modeSet',
      name: 'ModeSet',
      meta: { system: ['caisuan'], menuKey: 'mpcmaterail:config:getCustomizeSort', title: '模板自定义设置' },
      component: () => import('@/views/cs/setting/modeSet')
    },
    {
      path: 'synthesizeSet',
      name: 'SynthesizeSet',
      meta: { system: ['caisuan'], menuKey: 'complex:config:index', title: '测算设置' }, // 旧：综合价设置
      component: () => import('@/views/cs/setting/synthesizeSet')
    },
    {
      path: 'infoSet',
      name: 'InfoSet',
      meta: { system: ['caisuan'], menuKey: 'mpcsys:sett:getInitializationSettById', title: '数据采集配置周期' }, // 旧：信息价设置
      component: () => import('@/views/cs/setting/infoSet')
    },
    {
      path: 'periodicalSet',
      name: 'PeriodicalSet',
      meta: { system: ['caisuan'], menuKey: 'mpccollect:messagePrice:getReleaseMsgPriceSett', title: '发布管理设置' }, // 旧：期刊设置
      component: () => import('@/views/cs/setting/periodicalSet')
    },
    {
      path: 'userSet',
      name: 'UserSet',
      meta: { system: ['caisuan'], menuKey: 'sys:common:index', title: '企业类型设置' }, // 旧：采集员设置
      component: () => import('@/views/cs/setting/userSet')
    },
    {
      path: 'materialSet',
      name: 'MaterialSet',
      meta: { system: ['caisuan'], title: '材价设置' },
      component: () => import('@/views/cs/setting/materialSet')
    }
    // { // 其他设置还没配权限
    //   path: 'otherSet',
    //   name: 'otherSet',
    //   meta: { menuKey: 'to_mpc_home', title: '其他设置' },
    //   component: () => import('@/views/cs/setting/otherSet')
    // }
    ]
  },
  // 公共功能模块-----------------------------------------------------
  // 材价管理
  {
    path: '/publicModule/materialManage',
    component: Layout,
    alwaysShow: true,
    redirect: '/publicModule/materialManage/index',
    meta: { system: ['caisuan', 'zixun'], menuKey: 'matIndex', title: '材价管理', icon: 'iconfont icon-caijiaguanli' },
    children: [
      {
        path: 'index',
        name: 'materialManage',
        component: () => import('@/views/publicModule/materialManage'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'matIndex', title: '材价库', activeMenu: '/publicModule/materialManage/index' }
      },
      {
        path: 'projectDetails',
        name: 'projectDetails',
        component: () => import('@/views/publicModule/materialManage/projectDetails'),
        meta: { system: ['caisuan', 'zixun'], title: '项目详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      },
      {
        path: 'infoPriceDetails',
        name: 'infoPriceDetails',
        component: () => import('@/views/publicModule/materialManage/infoPriceDetails'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'mpccollect:messagePriceDetail:getMessagePriceDetailListPage', title: '信息价详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      },
      {
        path: 'averagePriceDetails',
        name: 'averagePriceDetails',
        component: () => import('@/views/publicModule/materialManage/averagePriceDetails'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'mpccollect:messageAvgPriceDetail:getAvgMessagePriceDetailListPage', title: '信息均价详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      },
      {
        path: 'marketPriceDetails',
        name: 'marketPriceDetails',
        component: () => import('@/views/publicModule/materialManage/marketPriceDetails'),
        meta: { system: ['caisuan', 'zixun'], title: '市场材价详情', activeMenu: '/publicModule/materialManage/index' },
        hidden: true
      },
      {
        path: 'checkMaterial',
        name: 'CheckMaterial',
        component: () => import('@/views/publicModule/materialManage/checkMaterial'),
        meta: { system: ['caisuan', 'zixun'], menuKey: '', title: '造价通查价' }
      },
      {
        path: 'checkResult',
        name: 'CheckResult',
        component: () => import('@/views/publicModule/materialManage/detail/checkResult'),
        meta: { system: ['caisuan', 'zixun'], title: '查价结果', activeMenu: '/publicModule/materialManage/checkMaterial' },
        hidden: true
      },
      {
        path: 'checkHistory',
        name: 'CheckHistory',
        component: () => import('@/views/publicModule/materialManage/detail/checkHistory'),
        meta: { system: ['caisuan', 'zixun'], title: '查价历史', activeMenu: '/publicModule/materialManage/checkMaterial' },
        hidden: true
      }
    ]
  },
  // 文档管理
  {
    path: '/publicModule/file',
    component: Layout,
    alwaysShow: true,
    redirect: '/publicModule/file/index',
    meta: { system: ['caisuan', 'zixun'], menuKey: 'file:index', title: '文档管理', icon: 'iconfont icon-wendangguanli' },
    // hidden: true,  // czhui
    children: [
      {
        path: 'index',
        name: 'File',
        component: () => import('@/views/file/index'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'file:getFilePageList:mat', title: '公共文档' }
      },
      {
        path: 'indexDetail',
        name: 'IndexDetail',
        component: () => import('@/views/file/detail/indexDetail'),
        meta: { system: ['caisuan', 'zixun'], title: '材价文档详情', activeMenu: '/publicModule/file/index' },
        hidden: true
      },
      {
        path: 'projectDetail',
        name: 'ProjectDetail',
        component: () => import('@/views/file/detail/projectDetail'),
        meta: { system: ['caisuan', 'zixun'], title: '项目文档详情', activeMenu: '/publicModule/file/index' },
        hidden: true
      },
      {
        path: 'businessDetail',
        name: 'BusinessDetail',
        component: () => import('@/views/file/detail/businessDetail'),
        meta: { system: ['caisuan', 'zixun'], title: '企业文档详情', activeMenu: '/publicModule/file/index' },
        hidden: true
      },
      {
        path: 'privateIndex',
        name: 'privateIndex',
        component: () => import('@/views/file/components/CommPage'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'file:getFilePageList:projectLibrary', title: '私人文档' },
        props: { sourceChannel: 4, hasBack: false }
      },
      {
        path: 'informationIndex',
        name: 'informationIndex',
        component: () => import('@/views/file/components/CommPage'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'file:getFilePageList:enterprise', title: '资讯中心' },
        props: { sourceChannel: 5, hasBack: false }
      }
    ]
  },
  // 供应商管理
  {
    path: '/publicModule/supplierlManage',
    component: Layout,
    redirect: '/publicModule/supplierlManage/supplierIndex',
    alwaysShow: true,
    meta: { system: ['caisuan', 'zixun'], menuKey: 'supplier:brand', title: '供应商管理', icon: 'iconfont icon-pinpaiku' },
    children: [
      // 暂时不需要
      // {
      //   path: 'index',
      //   name: 'Material',
      //   component: () => import('@/views/materialManage/index'),
      //   meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '材价库' }
      // },
      // {
      //   path: 'sameMaterial',
      //   name: 'SameMaterial',
      //   component: () => import('@/views/materialManage/sameMaterial'),
      //   meta: { system: ['caisuan', 'zixun'], menuKey: 'to_mpc_home', title: '相同材价', activeMenu: '/materialManage/index' },
      //   hidden: true
      // },

      {
        path: 'supplierIndex',
        name: 'Supplier',
        component: () => import('@/views/supplier/index'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'supplier:getSupplierPageList', title: '供应商库' }
      },
      {
        path: 'brandIndex',
        name: 'BrandIndex',
        component: () => import('@/views/supplier/brandIndex'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'brand:getBrandPageList', title: '品牌库' }
      },
      {
        path: 'addSupplier',
        name: 'AddSupplier',
        component: () => import('@/views/supplier/detail/addSupplier'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'supplier:addOrUpdateBrand', title: '添加供应商', activeMenu: '/publicModule/supplierlManage/supplierIndex' },
        hidden: true
      },
      {
        path: 'detail',
        name: 'supplierDetail',
        component: () => import('@/views/supplier/detail/supplierDetail'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'supplier:getSupplierInfo', title: '供应商详情', activeMenu: '/publicModule/supplierlManage/supplierIndex' },
        hidden: true
      },
      {
        path: 'brandDetail',
        name: 'BrandDetail',
        component: () => import('@/views/supplier/detail/brandDetail'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'supplier:getMatPricePage', title: '品牌库详情', activeMenu: '/publicModule/supplierlManage/brandIndex' },
        hidden: true
      }
    ]
  },
  // 询价管理
  {
    path: '/publicModule/inquiryManage',
    component: Layout,
    alwaysShow: true,
    redirect: '/publicModule/inquiryManage/index',
    meta: { system: ['caisuan', 'zixun'], menuKey: 'inquire:manage:index', title: '询价管理', icon: 'iconfont icon-xunjiaguanli' },
    children: [
      {
        path: 'index',
        name: 'Inquiry',
        component: () => import('@/views/inquiryManage/index'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'inquire:manage:getInquireList', title: '询价库' }
      },
      {
        path: 'toInquiry',
        name: 'ToInquiry',
        component: () => import('@/views/inquiryManage/toInquiry'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'inquire:manage:addInquireToZjt', title: '询价', activeMenu: '/publicModule/inquiryManage/index' },
        hidden: true
      },
      { // 为了让侧边栏选中询价审核
        path: 'editInquiry',
        name: 'EditInquiry',
        component: () => import('@/views/inquiryManage/toInquiry'),
        meta: { system: ['caisuan', 'zixun'], title: '编辑询价', activeMenu: '/publicModule/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'inquiryDetails',
        name: 'InquiryDetails',
        component: () => import('@/views/inquiryManage/inquiryDetails'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'inquire:manage:selectInquiry', title: '项目批次详情', activeMenu: '/publicModule/inquiryManage/index' },
        hidden: true
      },
      {
        path: 'inquiryExamine',
        name: 'InquiryExamine',
        component: () => import('@/views/inquiryManage/inquiryExamine'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'inquiryAudit:getInquiryAuditList', title: '询价审核' }
      },
      {
        path: 'examinePass',
        name: 'ExaminePass',
        component: () => import('@/views/inquiryManage/examinePass'),
        meta: { system: ['caisuan', 'zixun'], title: '审核通过数据', activeMenu: '/publicModule/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'examineFail',
        name: 'ExamineFail',
        component: () => import('@/views/inquiryManage/examineFail'),
        meta: { system: ['caisuan', 'zixun'], title: '审核不通过数据', activeMenu: '/publicModule/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'examinePend',
        name: 'ExaminePend',
        component: () => import('@/views/inquiryManage/examinePend'),
        meta: { system: ['caisuan', 'zixun'], title: '待审核数据', activeMenu: '/publicModule/inquiryManage/inquiryExamine' },
        hidden: true
      },
      {
        path: 'inquiryDrafts',
        name: 'InquiryDrafts',
        component: () => import('@/views/inquiryManage/inquiryDrafts'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'inquire:manage:getInquireDraftBoxList', title: '草稿箱' }
      },
      { // 为了让侧边栏选中草稿箱
        path: 'toInquiryDrafts',
        name: 'ToInquiryDrafts',
        component: () => import('@/views/inquiryManage/toInquiry'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'inquire:manage:addInquireToZjt', title: '询价', activeMenu: '/publicModule/inquiryManage/inquiryDrafts' },
        hidden: true
      },
      // 询价设置
      {
        path: 'inquirySetting',
        name: 'InquirySetting',
        component: () => import('@/views/inquiryManage/inquirySet/inquirySetting'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'inquirySet:upperLimit', title: '询价设置' }
      },
      {
        path: 'buyCombo',
        name: 'buyCombo',
        meta: { system: ['caisuan', 'zixun'], title: '购买套餐', activeMenu: '/publicModule/inquirySet/inquirySetting' },
        component: () => import('@/views/inquiryManage/inquirySet/buyCombo'),
        hidden: true
      },
      {
        path: 'order',
        name: 'order',
        meta: { system: ['caisuan', 'zixun'], title: '订单', activeMenu: '/publicModule/inquirySet/order' },
        component: () => import('@/views//inquiryManage/inquirySet/order'),
        hidden: true
      },
      {
        path: 'orderDetail',
        name: 'orderDetail',
        meta: { system: ['caisuan', 'zixun'], title: '订单信息', activeMenu: '/publicModule/inquirySet/orderDetail' },
        component: () => import('@/views//inquiryManage/inquirySet/orderDetail'),
        hidden: true
      },
      {
        path: 'orderText',
        name: 'orderText',
        meta: { system: ['caisuan', 'zixun'], title: '订单信息', activeMenu: '/publicModule/inquirySet/orderText' },
        component: () => import('@/views//inquiryManage/inquirySet/orderText'),
        hidden: true
      }
    ]
  },
  {
    path: '/publicModule/setting',
    component: Layout,
    redirect: '/publicModule/setting',
    hidden: true,
    meta: { system: ['caisuan', 'zixun'], menuKey: 'sys:common', icon: 'iconfont icon-caijiaguanli' },
    children: [
      {
        path: 'materialSet',
        name: 'materialSet',
        component: () => import('@/views/publicModule/setting/materialSet'),
        meta: { system: ['caisuan', 'zixun'], menuKey: 'sys:common', title: '材价设置' }
      }
    ]
  }
  // 没配 menuKey，不判断权限
  // 别加 path: '*' ，会影响切换系统，404放在路由守卫中判断
  // 404 page must be placed at the end !!!
  // { path: '*', redirect: '/404', meta: { system: ['main', 'caisuan', 'zixun'] }, hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// 重置路由，在退出或切换系统时调用
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
// 路由配置扁平化为对象，key为子父路由拼接后的path
function toflatRoutes(allRoutes, parentPath = '') {
  let temp = {}
  allRoutes.forEach(route => {
    const path = `${parentPath ? (parentPath === '/' ? '' : parentPath) + '/' : ''}${route.path}`
    temp[path] = {
      name: route.name,
      redirect: route.redirect,
      meta: route.meta
    }
    if (route.children?.length > 0) {
      temp = { ...temp, ...toflatRoutes(route.children, path) }
    }
  })
  return temp
}
// 可以在路由守卫中根据flatAllRoutes判断是否切换系统
export const flatAllRoutes = toflatRoutes([...constantRoutes, ...asyncRoutes])
// console.log(flatAllRoutes, 'flatAllRoutes')

export default router
