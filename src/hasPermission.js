/**
 * 自定义指令: v-has
 * 实现功能权限控制
 * 注：
 * 1. v-has最好不要和v-if用在同一个元素上，否则会使元素状态变化非常混乱不可预测。
 * 2. 如果元素用了v-if，建议将$_has(menuKey)加到判断条件中，不使用v-has
 * 3. 如果元素的兄弟元素用了v-for，建议用 v-if 而不是 v-has，否则会导致v-for渲染失效。例如材价设置>材价库设置
 * 4. 根据数组动态渲染的菜单，可用用计算属性进行过滤。例如单项工程库>单项工程详情
*/
import store from './store'
export default (Vue) => {
  /** 权限指令v-has,对按钮权限的控制 **/
  let id = 0
  Vue.directive('has', {
    inserted(el, binding) {
      // 获取按钮权限
      if (!Vue.prototype.$_has(binding.value)) {
        if (binding.modifiers.show) { // 用了修饰.show，该元素会显示但不能操作 例：v-has.show="'xxx'"
          // el.classList.add('no-permission')
          id++
          el.classList.add('no-permission--' + id) // 设置唯一类名方便获取新元素
          el.style.cursor = 'auto'
          const parentEl = el.parentNode // 先获取父元素
          // 移除dom事件
          const html = el.outerHTML
          el.outerHTML = html // 注意：el设置了outerHTML后，el这个变量依然会引用原来的dom元素
          // 获取新元素，增加拓展功能=================
          // 无权限提示
          const newEl = parentEl.querySelector('.no-permission--' + id)
          newEl.title = '暂无权限'
          newEl.onclick = function () {
            Vue.prototype.$message.warning('暂无权限')
          }
        } else {
          // 移除不匹配的按钮
          // 只是隐藏元素建议用v-if="$_has(menuKey)"   实践中v-has="menuKey" 会出现各种奇怪的问题
          // 指令只用v-has.show
          el.parentNode.removeChild(el)
        }
      }
    }
  })

  Vue.prototype.$_has = function (value) {
    // 从vuex中获取权限数组
    var permissionMap = store.getters.permissionMap
    // 匹配vuex中的数据中有没有匹配的值，有则为true
    // return permissionMap[value]
    return true
  }
}
