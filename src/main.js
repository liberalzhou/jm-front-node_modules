import '@/assets/iconfonts/iconfont.css'
import myPlugins from '@/components'
import CompleteDialog from '@/components/commdialogs/CompleteDialog'
// 全局确认弹窗
import ConfirmDialog from '@/components/commdialogs/ConfirmDialog'
import '@/icons'; // svg-icon
import '@/permission'; // permission control
import '@/styles/font/font.scss'; // 全局字体
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import '@/styles/index.scss'; // global css
import { formatNumber } from '@/utils/common'
import '@/utils/flexible'
import roles from '@/utils/roles'; // 路由权限
import ElTableInfiniteScroll from 'el-table-infinite-scroll'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'normalize.css/normalize.css'; // A modern alternative to CSS resets
import Vue from 'vue'
import Print from 'vue-print-nb'
import App from './App'
import hasPermission from './hasPermission'; //  按钮权限
import router from './router'
import store from './store'
Vue.use(Print)

Vue.prototype.$roles = roles
Vue.use(hasPermission)
Vue.use(myPlugins)

Vue.prototype.formatNumber = formatNumber

// import config from '@/api/config'
// Vue.prototype.$baseUrl = config.baseUrl

// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

Vue.use(ElementUI)
Vue.use(ElTableInfiniteScroll)

Vue.component('ConfirmDialog', ConfirmDialog)
Vue.component('CompleteDialog', CompleteDialog)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

