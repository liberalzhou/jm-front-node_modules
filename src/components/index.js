// 搜索组件
import searchs from '@/components/searchs';

export default {
  install:function(Vue){
    Vue.component('searchs',searchs); //第一个参数:组件名称,第二个参数:要注册的组件
  }
}
