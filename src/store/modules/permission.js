import { asyncRoutes, constantRoutes } from '@/router'

// 判断路由权限
function hasPermission(permissionMap, route) {
  if (route.meta && route.meta.menuKey) {
    return permissionMap[route.meta.menuKey]
  } else {
    return true
  }
}
// 递归遍历过滤 asyncRoutes
function filterAsyncRoutes(asyncRoutes, permissionMap) {
  const res = []
  asyncRoutes.forEach(route => {
    const temp = { ...route }
    if (hasPermission(permissionMap, temp)) {
      if (temp.children) {
        temp.children = filterAsyncRoutes(temp.children, permissionMap)
      }
      res.push(temp)
    }
  })

  return res
}

const state = {
  userCommonMenuList: [], // 主系统
  userMpcMenuList: [], // 采算编发
  userMenuList: [], // 咨询
  commonFunctionList: [], // 公共
  routes: [],
  addRoutes: [],
  permissionMap: {}
}

const mutations = {
  SET_USER_COMMON_MENU_LIST: (state, menuList) => {
    state.userCommonMenuList = menuList
  },
  SET_USER_MPC_MENU_LIST: (state, menuList) => {
    state.userMpcMenuList = menuList
  },
  SET_USER_MENU_LIST: (state, menuList) => {
    state.userMenuList = menuList
  },
  SET_USER_FUNCTION_LIST: (state, functionList) => {
    state.commonFunctionList = functionList
  },
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  },
  SET_PERMISSION_MAP: (state, permissionMap) => {
    state.permissionMap = permissionMap
  }
}

const actions = {
  // 创建路由配置和按钮权限表
  createPermission({ state, commit }, { system, menuList }) {
    return new Promise(resolve => {
      // 不需要区分路由和按钮，有一些按钮的功能就是链接，menuKey既是路由，也是按钮
      const permissionMap = {}
      menuList.forEach(item => {
        permissionMap[item.menuKey] = true
      })
      // // 路由配置
      // const accessedRoutes = filterAsyncRoutes(asyncRoutes, permissionMap) // 判断权限过滤路由
      const accessedRoutes = asyncRoutes.filter(route => route.meta?.system?.includes(system)) // 跳过判断权限（临时需要）

      commit('SET_ROUTES', accessedRoutes)

      // 权限
      commit('SET_PERMISSION_MAP', permissionMap)

      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
