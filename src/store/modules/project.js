import { queryStageList } from '@/api/newProject';

const getDefaultState = () => {
  return {
    projectCompare: JSON.parse(sessionStorage.getItem('project_params')) || {
      projectParams: [],  // 项目阶段数据数组
      stageId: null,      // 阶段id
      stageUseName: null, // 阶段名
      taxManner: null,    // 计税：1：增值税（一般计税方法），2：简易计税方法
      specialtyType: null,  // 专业分类1
      projectType: null,  // 项目分类2
      engType: null,  // 工程分类3
    }, // 项目对比详情参数
    stageCompare: JSON.parse(sessionStorage.getItem('stage_params')) || {
      projectName: '', // 项目名
      projectId: '',  // 项目id
      projectStageId: '', // 项目阶段id
      stageStatus: '',    // 阶段id
      taxManner: '',   // 计税方式
      projectStatus: [],  //项目分类: [房屋工程,居住建筑,住宅]
      areaStatus: [],     // 项目地点: [省,市,区]
    }, // 阶段对比详情参数
    projectVisible: JSON.parse(sessionStorage.getItem('project_visible')) || {
      projectId: '',  // 项目id
      stageId: '',  // 阶段id
      name: '', // 项目名称
      isTypical: false, // 是否典型
      projectStageList: [], // 项目阶段列表
    }, // 可视化数据
    stageList: [],  // 项目阶段列表
  }
}

const state = getDefaultState()

const mutations = {
  // 设置项目对比详情参数
  SET_PROJECT_COMPARE: (state, params) => {
    state.projectCompare = params
    sessionStorage.setItem('project_params', JSON.stringify(params))
  },
  // 设置阶段对比详情参数
  SET_STAGE_COMPARE: (state, params) => {
    state.stageCompare = params
    sessionStorage.setItem('stage_params', JSON.stringify(params))
  },
  // 设置项目可视化参数
  SET_PROJECT_VISIBLE: (state, params) => {
    state.projectVisible = params
    sessionStorage.setItem('project_visible', JSON.stringify(params))
  },
  SET_STAGE_LIST: (state, list) => {
    state.stageList = list;
  }
}

const actions = {
  getStageList({ commit}) {
    queryStageList().then(res => {
      if (res && res.code == '01') {
        commit('SET_STAGE_LIST', res.body)
      }
    })
  }
}

export default {
  state,
  mutations,
  actions
}

