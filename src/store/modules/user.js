import { login, logout } from '@/api/user'
import { clearStorage } from '@/utils/clearStorage'

const getDefaultState = () => {
  return {
    roles: '',
    btnRoles: [],
    userAccount: sessionStorage.getItem('user_account'),
    userName: sessionStorage.getItem('user_name'),
    // id: sessionStorage.getItem('id'),
    token: sessionStorage.getItem('token'),
    userId: '',
    userMenu: sessionStorage.getItem('userMenu') || '', // fj20220523
    userFlag: sessionStorage.getItem('userFlag') || '', // fj20220523
    sysLogo: sessionStorage.getItem('sysLogo'), // fj20220526
    sysId: sessionStorage.getItem('sysId'), // fj20220526
    phone: sessionStorage.getItem('phone'),
    userHead: sessionStorage.getItem('user_head'),
    department: sessionStorage.getItem('user_department'),
    position: sessionStorage.getItem('user_position'),
    qq: sessionStorage.getItem('user_qq'),
    belongCompany: sessionStorage.getItem('belong_company'),
    sysName: sessionStorage.getItem('sys_name'),
    sysUuid: sessionStorage.getItem('sys_uuid'),
    version: sessionStorage.getItem('version'),
    technicalSupport: sessionStorage.getItem('technical_support'),
    updateTime: sessionStorage.getItem('update_time'),
    city: sessionStorage.getItem('init_default_city') // 采算   初始化默认城市和省份
  }
}

const state = getDefaultState()

const mutations = {
  SET_DEFAULT_CITY: (state, city) => {
    state.city = city
  }, // 采算
  SET_USER_FLAG: (state, userFlag) => {
    state.userFlag = userFlag
  }, // fj20220523
  SET_USER_MENU: (state, userMenu) => {
    state.userMenu = userMenu
  }, // fj20220523
  SET_SYS_LOGO: (state, sysLogo) => {
    state.sysLogo = sysLogo
  }, // fj20220526
  SET_SYS_ID: (state, sysId) => {
    state.sysId = sysId
  }, // fj20220526
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_USER_ACCOUNT: (state, userAccount) => {
    state.userAccount = userAccount
  },
  SET_NAME: (state, name) => {
    state.userName = name
  },
  SET_ID: (state, id) => {
    state.userId = id
  },
  SET_PHONE: (state, phone) => {
    state.phone = phone
  },
  SET_USER_HEAD: (state, userHead) => {
    state.userHead = userHead
  },
  SET_USER_DEPARTMENT: (state, department) => {
    state.department = department
  },
  SET_USER_POSITION: (state, position) => {
    state.position = position
  },
  SET_USER_QQ: (state, qq) => {
    state.qq = qq
  },
  SET_BTN_ROLES: (state, btnRoles) => {
    state.btnRoles = btnRoles
  },
  SET_BELONG_COMPANY: (state, belongCompany) => {
    state.belongCompany = belongCompany
  },
  SET_SYS_NAME: (state, sysName) => {
    state.sysName = sysName
  },
  SET_SYS_UUID: (state, sysUuid) => {
    state.sysUuid = sysUuid
  },
  SET_VERSION: (state, version) => {
    state.version = version
  },
  SET_TECHNICAL_SUPPORT: (state, technicalSupport) => {
    state.technicalSupport = technicalSupport
  },
  SET_UPDATE_TIME: (state, updateTime) => {
    state.updateTime = updateTime
  }
}

const actions = {
  // 用户登录
  login({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      login(userInfo).then(res => {
        if (res.code === '01') {
          // commit('SET_NAME', res.body.userInfoRes.userName)
          commit('SET_NAME', res.body.userInfoCommonRes.userName)
          // 路由守卫中判断是否已登录
          commit('SET_TOKEN', res.body.token)
          sessionStorage.setItem('token', res.body.token)
          resolve(res)
        } else {
          reject(res)
        }
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 退出登录
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      // 调用退出接口，无论是否成功，都清掉缓存让用户退出
      logout({ userId: state.userId })
      clearStorage()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

