const moduleTarget = {
    state: {
        selectedCondition: {},
        economicCategory: [],  // 经济 - 工程类别
        technologyCategory: [],  // 技术 - 工程类别
    },
    mutations: {
        SET_SELECTED_CONDITION(state, conditionObj) {
            state.selectedCondition = Object.assign({}, conditionObj);
        },
        SET_ECONOMIC_CATEGORY(state, economicCategory) {
            state.economicCategory = economicCategory;
        },
        SET_TECHNOLOGY_CATEGORY(state, technologyCategory) {
            state.technologyCategory = technologyCategory;
        }
    },
    getters: {
        selectedCondition(state) {
            return state.selectedCondition;
        },
        economicCategory(state) {
            return state.economicCategory;
        },
        technologyCategory(state) {
            return state.technologyCategory;
        }
    }
}

export default moduleTarget