import { resetRouter } from '@/router'
import store from '@/store'

// 清除vuex状态及缓存
export function clearStorage() {
  sessionStorage.removeItem('user_account')
  sessionStorage.removeItem('user_name')
  // sessionStorage.removeItem('id')
  sessionStorage.removeItem('token')
  sessionStorage.removeItem('phone')
  sessionStorage.removeItem('user_head')
  sessionStorage.removeItem('user_department')
  sessionStorage.removeItem('user_position')
  sessionStorage.removeItem('user_qq')
  sessionStorage.removeItem('belong_company')
  sessionStorage.removeItem('sys_name')
  sessionStorage.removeItem('sys_uuid')
  sessionStorage.removeItem('version')
  sessionStorage.removeItem('technical_support')
  sessionStorage.removeItem('update_time')
  sessionStorage.removeItem('userMenu')// fj20220523
  sessionStorage.removeItem('userFlag')// fj20220523
  sessionStorage.removeItem('sysLogo')// fj20220526
  sessionStorage.removeItem('sysId')// fj20220526
  sessionStorage.removeItem('init_default_city') // 采算
  sessionStorage.removeItem('currentSystem')
  store.commit('user/SET_DEFAULT_CITY', '') // 采算
  store.commit('user/SET_SYS_ID', '')// fj20220526
  store.commit('user/SET_SYS_LOGO', '')// fj20220526
  store.commit('user/SET_USER_FLAG', '')// fj20220523
  store.commit('user/SET_USER_MENU', '')// fj20220523
  store.commit('user/SET_USER_ACCOUNT', '')
  store.commit('user/SET_NAME', '')
  store.commit('user/SET_ID', '')
  store.commit('user/SET_PHONE', '')
  store.commit('user/SET_USER_HEAD', '')
  store.commit('user/SET_BTN_ROLES', [])
  store.commit('user/SET_USER_DEPARTMENT', '')
  store.commit('user/SET_USER_POSITION', '')
  store.commit('user/SET_USER_QQ', '')
  store.commit('user/SET_BELONG_COMPANY', '')
  store.commit('user/SET_SYS_NAME', '')
  store.commit('user/SET_SYS_UUID', '')
  store.commit('user/SET_VERSION', '')
  store.commit('user/SET_TECHNICAL_SUPPORT', '')
  store.commit('user/SET_UPDATE_TIME', '')
  store.commit('user/SET_TOKEN', '')
  resetRouter()
}
