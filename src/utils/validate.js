
/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}


/* 是否手机号码或者固话*/
export function validatePhoneTwo(rule, value, callback) {
  const reg = /^((0\d{2,3}-\d{7,8})|(1[34578]\d{9}))$/;;
  if (value == '' || value == undefined || value == null) {
      callback();
  } else {
      if ((!reg.test(value)) && value != '') {
          callback(new Error('请输入正确的电话号码或者固话号码'));
      } else {
          callback();
      }
  }
}

/* 是否手机号码*/
export function validatePhone(rule, value, callback) {
  const reg = /^[1][3,4,5,7,8][0-9]{9}$/;
  if (value == '' || value == undefined || value == null) {
      callback();
  } else {
      if ((!reg.test(value)) && value != '') {
          callback(new Error('请输入正确的电话号码'));
      } else {
          callback();
      }
  }
}

/* 是否手机号码--多个-用竖线分隔*/
export function validatePhoneMultiple(rule, value, callback) {
  const reg = /^[1][3,4,5,7,8][0-9]{9}$/;
  if (value == '' || value == undefined || value == null) {
      callback();
  } else {
      if (value.indexOf("|") != -1) {
          let arr = value.split("|");
          for (let i = 0; i < arr.length; i++) {
              if ((!reg.test(arr[i])) && arr[i] != '') {
                  callback(new Error(`第${i + 1}个电话号码输入有误`));
                  return
              }
          }
          callback();
      } else {
          if ((!reg.test(value)) && value != '') {
              callback(new Error('请输入正确的电话号码'));
          } else {
              callback();
          }
      }

  }
}

/* 是否身份证号码*/
export function validateIdNo(rule, value, callback) {
  const reg = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[X])$)$/;
  if (value == '' || value == undefined || value == null) {
      callback();
  } else {
      if ((!reg.test(value)) && value != '') {
          callback(new Error('请输入正确的身份证号码'));
      } else {
          callback();
      }
  }
}

/* 校验正确的微信号*/
export function validateWeChart(rule, value, callback) {
  const reg = /^[a-zA-Z]{1}[-_a-zA-Z0-9]{5,19}$/;
  if (value == '' || value == undefined || value == null) {
      callback();
  } else {
      if ((!reg.test(value)) && value != '') {
          callback(new Error('请输入正确的微信号'));
      } else {
          callback();
      }
  }
}

/* 校验正确的邮箱格式*/
export function validateEmail(rule, value, callback) {
  const reg = /^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;
  if (value == '' || value == undefined || value == null) {
      callback();
  } else {
      if (value != '' && !reg.test(value)) {
          callback(new Error('请输入正确的邮箱格式'));
      } else {
          callback();
      }
  }
}