let conditionList = {
    samples: {
        '指标来源': [
            {
                id: 1,
                name: '原项目造价指标'
            },
            {
                id: 2,
                name: '公有云共享指标'
            }
        ],
        '阶段': [
            {
                id: 3,
                name: '估算'
            },
            {
                id: 4,
                name: '概算'
            },
            {
                id: 5,
                name: '预算'
            },
            {
                id: 6,
                name: '结算'
            }
        ],
    },
    features: {
        '工程类别': [
            {
                id: 7,
                name: '新建工程'
            },
            {
                id: 8,
                name: '改建工程'
            },
            {
                id: 9,
                name: '迁建工程'
            },
            {
                id: 10,
                name: '扩建工程'
            },
            {
                id: 11,
                name: '恢复工程'
            }
        ],
        '基础类型': [
            {
                id: 12,
                name: '独立基础'
            },
            {
                id: 13,
                name: '条形基础'
            },
            {
                id: 14,
                name: '满堂基础'
            },
            {
                id: 15,
                name: '桩基础'
            },
            {
                id: 16,
                name: '桩成台基础'
            },
            {
                id: 17,
                name: '混合基础'
            },
            {
                id: 18,
                name: '带行基础'
            },
            {
                id: 19,
                name: '筏板基础'
            },
            {
                id: 20,
                name: '砖基础'
            }
        ],
        '结构类型': [
            {
                id: 21,
                name: '框架结构'
            },
            {
                id: 22,
                name: '剪力墙结构'
            },
            {
                id: 23,
                name: '框架剪力墙结构'
            },
            {
                id: 24,
                name: '短肢剪力墙结构'
            },
            {
                id: 25,
                name: '砖混结构'
            },
            {
                id: 26,
                name: '钢结构'
            },
            {
                id: 27,
                name: '装配式混凝土结构'
            }
        ],
        '装修标准': [
            {
                id: 28,
                name: '毛坯'
            },
            {
                id: 29,
                name: '简装'
            },
            {
                id: 30,
                name: '精装'
            },
            {
                id: 31,
                name: '单装修'
            }
        ],
        '绿色建筑等级': [
            {
                id: 32,
                name: '一星'
            },
            {
                id: 33,
                name: '二星'
            },
            {
                id: 34,
                name: '三星'
            }
        ],
        '抗震等级': [
            {
                id: 35,
                name: '一级'
            },
            {
                id: 36,
                name: '二级'
            },
            {
                id: 37,
                name: '三级'
            },
            {
                id: 38,
                name: '四级'
            }
        ]
    }
}

let projectType = [
    {
        id: 01,
        name: '单项工程指标',
        subs: [
            {
                id: 101,
                name: '单项工程造价'
            },
            {
                id: 102,
                name: '主材总造价',
                subs: [
                    {
                        id: 1001,
                        name: '人工'
                    },
                    {
                        id: 1002,
                        name: '钢筋'
                    },
                    {
                        id: 1003,
                        name: '水泥'
                    },
                    {
                        id: 1004,
                        name: '混凝土'
                    }
                ]
            }
        ]
    },
    {
        id: 02,
        name: '单位工程指标',
        subs: [
            {
                id: 103,
                name: '土建工程',
                subs: [
                    {
                        id: 1005,
                        name: '分部分项',
                        subs: [
                            {
                                id: 10001,
                                name: '土石方工程',
                                subs: [
                                    {
                                        id: 100001,
                                        name: '土方工程',
                                        subs: [
                                            {
                                                id: 1000001,
                                                name: '平整场地'
                                            },
                                            {
                                                id: 1000002,
                                                name: '挖一般土方'
                                            },
                                            {
                                                id: 1000003,
                                                name: '挖沟槽土方'
                                            },
                                            {
                                                id: 1000004,
                                                name: '冻土开挖'
                                            },
                                            {
                                                id: 1000005,
                                                name: '挖淤泥、流沙'
                                            },
                                            {
                                                id: 1000006,
                                                name: '管沟土方'
                                            }
                                        ]
                                    },
                                    {
                                        id: 100002,
                                        name: '石方工程'
                                    },
                                    {
                                        id: 100003,
                                        name: '回填工程'
                                    }
                                ]
                            },
                            {
                                id: 10002,
                                name: '桩基工程'
                            },
                            {
                                id: 10003,
                                name: '砌筑工程'
                            },
                            {
                                id: 10004,
                                name: '门窗工程'
                            }
                        ]
                    },
                    {
                        id: 1006,
                        name: '工料机',
                        subs: [
                            {
                                id: 10005,
                                name: '人工'
                            },
                            {
                                id: 10006,
                                name: '材料',
                                subs: [
                                    {
                                        id: 100004,
                                        name: '钢筋'
                                    },
                                    {
                                        id: 100005,
                                        name: '混凝土'
                                    },
                                    {
                                        id: 100006,
                                        name: '水泥'
                                    }
                                ]
                            },
                            {
                                id: 10007,
                                name: '机械'
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

let unitProject = ['房屋建筑与装饰工程', '仿古建筑工程', '通用安装工程', '市政工程', '园林绿化工程', '矿山工程', '构筑工程', '城市轨道交通工程', '爆破工程']

let featureList = [
    {
        fieldName: "工程类别",
        predict: "新建",
        probably: "改建",
        budget: "扩建",
        settle: "恢复",
    },
    {
        fieldName: "基础类型",
        predict: "独立",
        probably: "条形",
        budget: "混合",
        settle: "桩",
    },
    {
        fieldName: "结构类型",
        predict: "框架",
        probably: "钢",
        budget: "剪力墙",
        settle: "装配式混凝土",
    },
    {
        fieldName: "装修标准",
        predict: "毛坯",
        probably: "公共区域带装修+户内毛坯",
        budget: "单简装",
        settle: "毛坯带精装",
    },
    {
        fieldName: "绿色建筑等级",
        predict: "基本级",
        probably: "一星",
        budget: "一星",
        settle: "二星",
    },
    {
        fieldName: "抗震等级",
        predict: "特一级",
        probably: "一级",
        budget: "三级",
        settle: "非抗震",
    },
    {
        fieldName: "楼层类型",
        predict: "低层",
        probably: "低层",
        budget: "多层",
        settle: "多层",
    },
    {
        fieldName: "楼高或房屋高度",
        predict: "10",
        probably: "12",
        budget: "12",
        settle: "12",
    },
    {
        fieldName: "地下层数",
        predict: "1",
        probably: "1",
        budget: "1",
        settle: "1",
    },
    {
        fieldName: "地上层数",
        predict: "3",
        probably: "3",
        budget: "3",
        settle: "3",
    },
    {
        fieldName: "地下层高",
        predict: "3",
        probably: "3",
        budget: "3",
        settle: "3",
    },
    {
        fieldName: "地上层高",
        predict: "9",
        probably: "9",
        budget: "9",
        settle: "9",
    },
    {
        fieldName: "地下建筑面积",
        predict: "100",
        probably: "100",
        budget: "120",
        settle: "120",
    },
    {
        fieldName: "地上建筑面积",
        predict: "120",
        probably: "120",
        budget: "120",
        settle: "120",
    },
    {
        fieldName: "总建筑面积",
        predict: "4800",
        probably: "4800",
        budget: "4800",
        settle: "4800",
    }
]

let stageList = [
    {
        fieldName: "价格取定期",
        predict: "",
        probably: "",
        budget: "",
        settle: "",
    },
    {
        fieldName: "计税方式",
        predict: "",
        probably: "",
        budget: "",
        settle: "",
    },
    {
        fieldName: "计价依据",
        predict: "",
        probably: "",
        budget: "",
        settle: "",
    },
    {
        fieldName: "总造价",
        predict: "",
        probably: "",
        budget: "",
        settle: "",
    },
    {
        fieldName: "单方造价",
        predict: "",
        probably: "",
        budget: "",
        settle: "",
    },
    {
        fieldName: "建筑面积",
        predict: "",
        probably: "",
        budget: "",
        settle: "",
    },
]

let subItemList = [
    {
        id: 'A01',
        name: '土石方工程',
        estimateCost: '',
        estimateTarget: '',
        probablyCost: '',
        probablyTarget: '',
        budgetCost: '',
        budgetTarget: '',
        settleCost: '',
        settleTarget: '',
        finalCost: '',
        finalTarget: '',
        childs: [
            {
                id: 'B01',
                name: '土方工程',
                estimateCost: '',
                estimateTarget: '',
                probablyCost: '',
                probablyTarget: '',
                budgetCost: '',
                budgetTarget: '',
                settleCost: '',
                settleTarget: '',
                finalCost: '',
                finalTarget: '',
                childs: [
                    {
                        id: 'C01',
                        name: '挖一般土方',
                        estimateCost: '',
                        estimateTarget: '',
                        probablyCost: '',
                        probablyTarget: '',
                        budgetCost: '',
                        budgetTarget: '',
                        settleCost: '',
                        settleTarget: '',
                        finalCost: '',
                        finalTarget: '',
                        grade3: true
                    },
                    {
                        id: 'C02',
                        name: '平整场地',
                        estimateCost: '',
                        estimateTarget: '',
                        probablyCost: '',
                        probablyTarget: '',
                        budgetCost: '',
                        budgetTarget: '',
                        settleCost: '',
                        settleTarget: '',
                        finalCost: '',
                        finalTarget: '',
                        grade3: true
                    }
                ]
            }
        ]
    },
    {
        id: 'A02',
        name: '回填',
        estimateCost: '',
        estimateTarget: '',
        probablyCost: '',
        probablyTarget: '',
        budgetCost: '',
        budgetTarget: '',
        settleCost: '',
        settleTarget: '',
        finalCost: '',
        finalTarget: '',
        childs: []
    },
    {
        id: 'A03',
        name: '砌筑工程',
        estimateCost: '',
        estimateTarget: '',
        probablyCost: '',
        probablyTarget: '',
        budgetCost: '',
        budgetTarget: '',
        settleCost: '',
        settleTarget: '',
        finalCost: '',
        finalTarget: '',
        childs: []
    },
    {
        id: 'A04',
        name: '门窗工程',
        estimateCost: '',
        estimateTarget: '',
        probablyCost: '',
        probablyTarget: '',
        budgetCost: '',
        budgetTarget: '',
        settleCost: '',
        settleTarget: '',
        finalCost: '',
        finalTarget: '',
        childs: []
    },
    {
        id: 'A05',
        name: '天棚工程',
        estimateCost: '',
        estimateTarget: '',
        probablyCost: '',
        probablyTarget: '',
        budgetCost: '',
        budgetTarget: '',
        settleCost: '',
        settleTarget: '',
        finalCost: '',
        finalTarget: '',
        childs: []
    }
]

let contrastData = {
    tHeaderList: [
        {
            label: '对比项目',
            prop: 'fieldName',
            disable: true
        },
        {
            label: '项目地点',
            prop: 'address',
        },
        {
            label: '项目分类',
            prop: 'projectType',
        }
    ],
    dataList: [
        {
            projectName: '项目1',
            address: "广东省广州市",
            projectType: "土建工程"
        },
        {
            projectName: '项目2',
            address: "广东省深圳市",
            projectType: "安装工程"
        },
        {
            projectName: '项目3',
            address: "广西省柳州市",
            projectType: "土建工程"
        }
    ]
}

let contrastFeature = {
    tHeaderList: [
        {
            label: '对比项目',
            prop: 'fieldName',
            disable: true
        },
        {
            label: '工程类别',
            prop: 'projectType',
        },
        {
            label: '基础类型',
            prop: 'baseType',
        },
        {
            label: '结构类型',
            prop: 'structType',
        },
        {
            label: '装修标准',
            prop: 'standard',
        },
        {
            label: '绿色建筑等级',
            prop: 'greenGrade',
        },
        {
            label: '抗震等级',
            prop: 'seismicGrade',
        },
        {
            label: '楼层类型',
            prop: 'floorType',
        },
        {
            label: '楼高或房屋高度',
            prop: 'buildHeight',
        },
        {
            label: '地下层级',
            prop: 'underLevel',
        },
        {
            label: '地上层数',
            prop: 'upLevel',
        },
        {
            label: '地下层高',
            prop: 'underHeight',
        },
        {
            label: '地上层高',
            prop: 'upHeight',
        },
        {
            label: '地下建筑面积',
            prop: 'underArea',
        },
        {
            label: '地上建筑面积',
            prop: 'upArea',
        },
        {
            label: '总建筑面积',
            prop: 'totalArea',
        },
    ],
    dataList: [
        {
            projectName: '项目1',
            projectType: "",
            baseType: "",
            structType: "",
            standard: "",
            greenGrade: "",
            seismicGrade: "",
            floorType: "",
            buildHeight: "",
            underLevel: "",
            upLevel: "",
            underHeight: "",
            upHeight: "",
            underArea: "",
            upArea: "",
            totalArea: ""
        },
        {
            projectName: '项目2',
            projectType: "",
            baseType: "",
            structType: "",
            standard: "",
            greenGrade: "",
            seismicGrade: "",
            floorType: "",
            buildHeight: "",
            underLevel: "",
            upLevel: "",
            underHeight: "",
            upHeight: "",
            underArea: "",
            upArea: "",
            totalArea: ""
        },
        {
            projectName: '项目3',
            projectType: "",
            baseType: "",
            structType: "",
            standard: "",
            greenGrade: "",
            seismicGrade: "",
            floorType: "",
            buildHeight: "",
            underLevel: "",
            upLevel: "",
            underHeight: "",
            upHeight: "",
            underArea: "",
            upArea: "",
            totalArea: ""
        }
    ]
}

let contrastStage = {
    tHeaderList: [
        {
            label: '对比项目',
            prop: 'fieldName',
            disable: true
        },
        {
            label: '造价阶段',
            prop: 'costStage',
        },
        {
            label: '价格取定期',
            prop: 'priceFixed',
        },
        {
            label: '计税方式',
            prop: 'taxMethod',
        },
        {
            label: '计价依据',
            prop: 'valueBasis',
        },
        {
            label: '总造价',
            prop: 'totalCost',
        },
        {
            label: '单方造价',
            prop: 'economy',
        },
        {
            label: '建筑面积',
            prop: 'caliber',
        },
    ],
    dataList: [
        {
            projectName: '项目1',
            costStage: "",
            priceFixed: "",
            taxMethod: "",
            valueBasis: "",
            totalCost: "",
            economy: "",
            caliber: "",
        },
        {
            projectName: '项目2',
            costStage: "",
            priceFixed: "",
            taxMethod: "",
            valueBasis: "",
            totalCost: "",
            economy: "",
            caliber: "",
        },
        {
            projectName: '项目3',
            costStage: "",
            priceFixed: "",
            taxMethod: "",
            valueBasis: "",
            totalCost: "",
            economy: "",
            caliber: "",
        }
    ]
}

let contrastUnit = {
    "project": [
        {
            "id": 1,
            "name": "项目名1",
            "project": [{
                "id": 101,
                "name": "土建工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 1001,
                    "name": "分部分项费",
                    "price": 100,
                    "econStd": 10.5
                }, {
                    "id": 1002,
                    "name": "措施合计费",
                    "price": 100,
                    "econStd": 10.5
                }]
            }, {
                "id": 102,
                "name": "安装工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 1003,
                    "name": "安装工程子项1",
                    "price": 100,
                    "econStd": 10.5
                }, {
                    "id": 1004,
                    "name": "安装工程子项2",
                    "price": 100,
                    "econStd": 10.5
                }]
            }]
        },
        {
            "id": 2,
            "name": "项目名2",
            "project": [{
                "id": 201,
                "name": "土建工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 2001,
                    "name": "分部分项费",
                    "price": 100,
                    "econStd": 10.5
                }, {
                    "id": 2002,
                    "name": "措施合计费",
                    "price": 100,
                    "econStd": 10.5
                }]
            }, {
                "id": 202,
                "name": "安装工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 2003,
                    "name": "安装工程子项1",
                    "price": 100,
                    "econStd": 10.5
                }, {
                    "id": 2004,
                    "name": "安装工程子项2",
                    "price": 100,
                    "econStd": 10.5
                }]
            }]
        }
    ]
}

let contrastSub = {
    "project": [
        {
            "id": 1,
            "name": "项目名1",
            "project": [{
                "id": 101,
                "name": "土方石工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 1001,
                    "name": "土方工程",
                    "price": 100,
                    "econStd": 10.5,
                    "project": [{
                        "id": 10001,
                        "name": "挖一般土方",
                        "price": 100,
                        "econStd": 10.5,
                    },{
                        "id": 10002,
                        "name": "平整场地",
                        "price": 100,
                        "econStd": 10.5,
                    }]
                }, {
                    "id": 1002,
                    "name": "回填",
                    "price": 100,
                    "econStd": 10.5
                }]
            }, {
                "id": 102,
                "name": "砌筑工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 1003,
                    "name": "砌筑工程子项1",
                    "price": 100,
                    "econStd": 10.5
                }, {
                    "id": 1004,
                    "name": "砌筑工程子项2",
                    "price": 100,
                    "econStd": 10.5
                }]
            }]
        },
        {
            "id": 2,
            "name": "项目名2",
            "project": [{
                "id": 201,
                "name": "土方石工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 2001,
                    "name": "土方工程",
                    "price": 100,
                    "econStd": 10.5
                }, {
                    "id": 2002,
                    "name": "回填",
                    "price": 100,
                    "econStd": 10.5
                }]
            }, {
                "id": 202,
                "name": "砌筑工程",
                "price": 100,
                "econStd": 10.5,
                "project": [{
                    "id": 2003,
                    "name": "砌筑工程子项1",
                    "price": 100,
                    "econStd": 10.5
                }, {
                    "id": 2004,
                    "name": "砌筑工程子项2",
                    "price": 100,
                    "econStd": 10.5
                }]
            }]
        }
    ]
}

let mainCategories = [
    {
        code: "01",
        name: "黑色有色金属",
        sub: [
            {
                code: "A01",
                name: "钢筋"
            },
            {
                code: "A02",
                name: "钢丝"
            },
            {
                code: "A03",
                name: "圆钢"
            }
        ]
    },
    {
        code: "02",
        name: "水泥、砖瓦灰砂石及混凝土制品",
        sub: [
            {
                code: "B01",
                name: "水泥"
            },
            {
                code: "B02",
                name: "砂"
            },
            {
                code: "B03",
                name: "石子"
            },
            {
                code: "B04",
                name: "轻骨料"
            }
        ]
    },
    {
        code: "03",
        name: "周转材料及五金工具",
        sub: [
            {
                code: "C01",
                name: "紧固件"
            },
            {
                code: "C02",
                name: "门窗五金"
            },
            {
                code: "C03",
                name: "家具五金"
            },
            {
                code: "C04",
                name: "五金配件"
            }
        ]
    }
]

module.exports = {
    conditionList,
    projectType,
    unitProject,
    featureList,
    stageList,
    subItemList,
    contrastData,
    contrastFeature,
    contrastStage,
    contrastUnit,
    contrastSub,
    mainCategories
}
