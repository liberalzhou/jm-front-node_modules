// 格式化日期YYYY-MM-DD
export function formatDate(curDate, type) {
  if (!curDate) {
    return null
  }
  // 兼容ie，ios浏览器，将YYYY-MM-DD转换成YYYY/MM/DD格式
  try {
    if (curDate.indexOf('-') > -1 && curDate.indexOf('T') == -1) {
      curDate = curDate.replace(/\-/g, '/')
      curDate = curDate.replace('.0', '')// 这里是将".0" 去掉，因为.0依旧会影响ios对日期的解析
    }
  } catch (err) {
    // console.log('err', curDate)
  }

  try {
    var date = new Date(curDate)
  } catch (err) {
    return null
  }

  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var hours = date.getHours()
  var minutes = date.getMinutes()
  var seconds = date.getSeconds()

  if (month < 10) {
    month = '0' + month
  }
  if (day < 10) {
    day = '0' + day
  }
  if (hours < 10) {
    hours = '0' + hours
  }
  if (minutes < 10) {
    minutes = '0' + minutes
  }
  if (seconds < 10) {
    seconds = '0' + seconds
  }

  // type = 1 格式返回：YYYY-MM-DD hh:mm:ss
  if (type == '1') {
    return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
  }
  // type = 2 格式返回：YYYY-MM-DD
  if (type == '2') {
    return year + '-' + month + '-' + day
  }
  // type = 3 格式返回：YYYY年MM月DD日
  if (type == '3') {
    return year + '年' + month + '月' + day + '日'
  }
  // type = 4 格式返回：YYYY年MM月
  if (type == '4') {
    return year + '年' + month + '月'
  }
  // type = 5
  if (type == '5') {
    return year
  }

  return year + '-' + month
}

/**
 * 获取某月的最后一天
 * @param {string} date (例：'2022-02-12'、'2022-02')
 * @return {string} (例：'2022-02-28')
 */
export function lastDayOfMonth(date) {
  const temp = date.split('-')
  temp[2] = new Date(temp[0], temp[1], 0).getDate()
  return temp.join('-')
}

/**
 * 获取某季度的最后一月
 * @param {string} date (例：'2022-02-12'、'2022-02')
 * @return {string} (例：'2022-03-12'、'2022-03')
 */
export function lastMonthOfQuarter(date) {
  const temp = date.split('-')
  const quarter = Math.floor((temp[1] - 1) / 3) + 1 // 从1开始，到4
  let qLastMonth = quarter * 3
  if (qLastMonth < 10) { // 补零
    qLastMonth = '0' + qLastMonth
  }
  temp[1] = qLastMonth
  return temp.join('-')
}

/**
 * 获取某季度的最后一天
 * @param {string} date (例：'2022-02-12'、'2022-02')
 * @return {string} (例：'2022-03-31')
 */
export function lastDayOfQuarter(date) {
  return lastDayOfMonth(lastMonthOfQuarter(date))
}

// 判断是否是闰年
export function isRunYear(year) {
  // 如果是闰年返回true，否则返回false
  let flag = false
  if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
    flag = true
  }
  return flag
}

// 验证手机号是否正确
export function validPhone(phone) {
  var reg = /^[1][3,4,5,7,8,9][0-9]{9}$/
  return reg.test(phone)
}

// 验证邮箱是否正确
export function validEmail(email) {
  var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/
  return reg.test(email)
}

// 数值输入限制
export function checkNumber(price) {
  // 清除"数字"和"."以外的字符
  price = price.replace(/[^\d.]/g, '')
  // 验证第一个字符是数字而不是字符
  price = price.replace(/^\./g, '')
  // 只保留第一个.清除多余的
  price = price.replace(/\.{2,}/g, '.')
  price = price.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.')
  // 只能输入两个小数
  price = price.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3')
  return price
}

/**
 * 数值以,分隔(例：12,345,678.90)
 * @param {number/string} value
 * @param {boolean} hasUnit 该字段为true，则代表结尾可能带有单位，即非数字的字符（例：99.9元/m）
 * @return {string}
 */
export function formatNumber(value, hasUnit) {
  let curValue = ''
  // 结尾非数值字符串
  let endStr = ''
  if (hasUnit) {
    let curValueArr = []
    try {
      curValueArr = value.split('')
    } catch (err) { }
    for (let i = 0; i < curValueArr.length; i++) {
      if (curValueArr[i] == '.' || !isNaN(Number(curValueArr[i]))) {
        curValue += curValueArr[i]
      } else {
        endStr = value.substring(i)
        break
      }
    }
  } else {
    curValue = value
  }
  if (isNaN(Number(curValue))) {
    return null
  }
  let curNumber = ''
  try {
    curNumber = curValue.toString()
  } catch (err) {
    return
  }

  // 整数部分
  const intSum = curNumber.split('.')[0].replace(/\B(?=(?:\d{3})+$)/g, ',')
  // 小数位
  let decimal = null
  if (curNumber.split('.')[1]) {
    decimal = curNumber.split('.')[1]
  }

  let res = intSum
  if (decimal) {
    res += '.' + decimal
  }
  if (hasUnit) {
    return res + endStr
  }

  return res
}

/**
 * 半角符号转换为全角符号
 */
export function toDBC(txtstring) {
  let tmp = ''
  for (var i = 0; i < txtstring.length; i++) {
    if (txtstring.charCodeAt(i) == 32) {
      tmp = tmp + String.fromCharCode(12288)
    } else if (txtstring.charCodeAt(i) < 127) {
      tmp = tmp + String.fromCharCode(txtstring.charCodeAt(i) + 65248)
    } else {
      tmp += txtstring.charAt(i)
    }
  }
  return tmp
}

/**
 * 根据 1920 宽度下页面的像素值，转换为当前窗口大小的对应像素值
 * 使用场景：当一些组件、图表库配置只能传入像素值时，需要自适应转换
 * @param {number} px
 * @returns {number}
 */
export function rem(px, min) {
  // 以1920窗口宽度为基准点，根元素字体大小为80
  let num = (px / 80) * parseFloat(document.documentElement.style.fontSize)
  if (min && num < min) {
    num = min
  }
  return num
}

/**
 * 拷贝或合并对象，原对象会被改变，因此如果不想改变原对象，target可传入{}
 * 参考了jQuery的extend方法实现
 * @param {Boolean} deep 是否深度操作
 * @param {Object} target 目标对象，接收源对象属性的对象，也是修改后的返回值。
 * @param {Object} source 源对象，包含将被合并的属性。
 * @returns {Object}
 * extendObject([deep,] target, source1[, source2, ...])
 */
export function extendObject() {
  // target 被扩展的对象
  // length 参数的数量
  // deep 是否深度操作
  var options; var name; var src; var copy; var copyIsArray; var clone
  var target = arguments[0] || {}
  var i = 1
  var length = arguments.length
  var deep = false

  // 如果只有一个参数，直接返回target
  if (length === i) {
    return target
  }
  // target为第一个参数，如果第一个参数是Boolean类型的值，则把target赋值给deep
  // deep表示是否进行深层面的复制，当为true时，进行深度复制，否则只进行第一层扩展
  // 然后把第二个参数赋值给target
  if (typeof target === 'boolean') {
    deep = target
    target = arguments[1] || {}

    // 将i赋值为2，跳过前两个参数
    i = 2
  }

  // target既不是对象也不是函数则把target设置为空对象。
  if (typeof target !== 'object' && !typeof target !== 'function') {
    target = {}
  }

  // 如果只有一个参数，则把jQuery对象赋值给target，即扩展到jQuery对象上
  // if (length === i) {
  //   target = this

  //   // i减1，指向被扩展对象
  //   --i
  // }

  // 开始遍历需要被扩展到target上的参数

  for (; i < length; i++) {
    // 处理第i个被扩展的对象，即除去deep和target之外的对象
    if ((options = arguments[i]) != null) {
      // 遍历第i个对象的所有可遍历的属性
      for (name in options) {
        // 根据被扩展对象的键获得目标对象相应值，并赋值给src
        src = target[name]
        // 得到被扩展对象的值
        copy = options[name]

        // 防止有环，例如 extendObject(true, target, {'target':target});
        if (target === copy) {
          continue
        }

        // 当用户想要深度操作时，递归合并
        // copy是纯对象或者是数组
        if (deep && copy && (Object.prototype.toString.call(copy) === '[object Object]' || (copyIsArray = Array.isArray(copy)))) {
          // 如果是数组
          if (copyIsArray) {
            // 将copyIsArray重新设置为false，为下次遍历做准备。
            copyIsArray = false
            // 判断被扩展的对象中src是不是数组
            clone = src && Array.isArray(src) ? src : []
          } else {
            // 判断被扩展的对象中src是不是纯对象
            clone = src && Object.prototype.toString.call(src) ? src : {}
          }

          // 递归调用extendObject方法，继续进行深度遍历
          target[name] = extendObject(deep, clone, copy)

          // 如果不需要深度复制，则直接把copy（第i个被扩展对象中被遍历的那个键的值）
        } else if (copy !== undefined) {
          target[name] = copy
        }
      }
    }
  }

  return target
}

/**
 * 深度优先，后序递归遍历
 * @param { Array } tree 树形结构的数组
 * @param { Function } func 节点执行函数
 * @param { Boolean } leaf 是否只对叶子节点操作
 */
export function lastTreeForeach(tree, func, children = 'children', leaf = false) {
  tree.forEach(data => {
    data[children] && lastTreeForeach(data[children], func, children, leaf) // 遍历子树
    if (leaf && data[children]?.length) {
      return
    }
    func(data)
  })
}

