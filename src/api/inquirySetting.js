import request from '@/utils/request'

// 询价设置----------------------------------------------------------------------

// 账号密码绑定接口
export function accountPswBind(data) {
  return request({
    url: '/inquirySet/accountPswBind',
    method: 'post',
    data
  })
}

// 获取绑定账号历史列表
export function getBindAccountHistoryList() {
  return request({
    url: '/inquirySet/getBindAccountHistoryList',
    method: 'post'
  })
}

// 获取短信验证码
export function getMobileCode(params) {
  return request({
    url: '/inquirySet/getMobileCode',
    method: 'post',
    params
  })
}

// 获取订单列表
export function getRechargeOrderPageList(data) {
  return request({
    url: '/inquirySet/getRechargeOrderPageList',
    method: 'post',
    data
  })
}

// 获取微信二维码
export function getTempORCodeInMem() {
  return request({
    url: '/inquirySet/getTempORCodeInMem',
    method: 'post'
  })
}

// 手机验证码绑定接口
export function mobileCodeBind(params) {
  return request({
    url: '/inquirySet/mobileCodeBind',
    method: 'post',
    params
  })
}

// 获取验证码
export function authImg() {
  return request({
    url: '/inquirySet/passport/authImg',
    method: 'get',
    responseType: 'arraybuffer'
  })
}

// qq登录回调接口
export function qqLoginCallBack(data) {
  return request({
    url: '/inquirySet/qqLoginCallBack',
    method: 'post',
    data
  })
}

// 微信扫码登录接口
export function qrLoginCheck(params) {
  return request({
    url: '/inquirySet/qrLoginCheck',
    method: 'post',
    params
  })
}

// 设置默认询价账号
export function setDefaultAccount(params) {
  return request({
    url: '/inquirySet/setDefaultAccount',
    method: 'post',
    params
  })
}

// 解绑账号
export function unBindAccount(params) {
  return request({
    url: '/inquirySet/unBindAccount',
    method: 'post',
    params
  })
}

// 同步历史询价详情
export function historicalInquiryDetails(data) {
  return request({
    url: '/inquirySet/historicalInquiryDetails',
    method: 'post',
    data
  })
}

// 同步历史询价列表
export function historicalInquiryList(data) {
  return request({
    url: '/inquirySet/historicalInquiryList',
    method: 'post',
    data
  })
}

// 询价库-------------------------------------------------------------------------------------------------

// 获取询价库列表
export function getInquireList(data) {
  return request({
    url: '/inquire/manage/getInquireList',
    method: 'post',
    data
  })
}

// 获取批次状态列表
export function getBatchStatusList() {
  return request({
    url: '/inquire/manage/getBatchStatusList',
    method: 'get'
  })
}

// 清单询价附件上传
export function inquiryUploadAnnex(data) {
  return request({
    url: '/inquire/manage/inquiryUploadAnnex',
    method: 'post',
    data
  })
}

// 导入清单询价
export function importDetailInquiry(data) {
  return request({
    url: '/inquire/manage/importDetailInquiry',
    method: 'post',
    data
  })
}

// 询价模板下载
export function downloadTempFile() {
  return request({
    url: '/inquire/manage/downloadTempFile',
    method: 'get',
    responseType: 'blob'
  })
}

// 获取绑定账号列表
export function getBindAccountList() {
  return request({
    url: '/inquirySet/getBindAccountList',
    method: 'post'
  })
}

// 获取所有省
export function getProvince() {
  return request({
    url: '/inquire/manage/getProvince',
    method: 'post'
  })
}

// 获取所有市
export function getCity(params) {
  return request({
    url: '/inquire/manage/getCity',
    method: 'post',
    params
  })
}

// 设置询价条数审核上限和开关
export function upperLimit(data) {
  return request({
    url: '/inquirySet/upperLimit',
    method: 'post',
    data
  })
}

// 获取询价条数审核上限和开关
export function getUpperLimit() {
  return request({
    url: '/inquire/manage/getUpperLimit',
    method: 'get'
  })
}

// 提交询价至造价通
export function addInquireToZjt(data) {
  return request({
    url: '/inquire/manage/addInquireToZjt',
    method: 'post',
    data
  })
}

// 接收造价通返回的询价数据
export function addInquireMaterialInfo(data) {
  return request({
    url: '/inquire/manage/addInquireMaterialInfo',
    method: 'post',
    data
  })
}

// 提交询价至草稿箱
export function addInquireToDrafts(data) {
  return request({
    url: '/inquire/manage/addInquireToDrafts',
    method: 'post',
    data
  })
}

// 删除草稿箱
export function deleteDrafts(params) {
  return request({
    url: '/inquire/manage/deleteDrafts',
    method: 'post',
    params
  })
}

// 审核-导出
export function exportInquiry(data) {
  return request({
    url: '/inquire/manage/exportInquiry',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 获取造价通状态
export function getZjtState(params) {
  return request({
    url: '/inquire/manage/getZjtState',
    method: 'post',
    params
  })
}

// 询价加急
export function inquiryUrgent(params) {
  return request({
    url: '/inquire/manage/inquiryUrgent',
    method: 'post',
    params
  })
}

// 查询询价详情
export function selectInquiry(data) {
  return request({
    url: '/inquire/manage/selectInquiry',
    method: 'post',
    data
  })
}

// 造价通返回的报价数
export function updateInquiryNumber(params) {
  return request({
    url: '/inquire/manage/updateInquiryNumber',
    method: 'post',
    params
  })
}

// 套餐列表
export function packageList(data) {
  return request({
    url: '/inquirySet/packageList',
    method: 'post',
    data
  })
}

// 套餐详情
export function packageDetails(params) {
  return request({
    url: '/inquirySet/packageDetails',
    method: 'get',
    params
  })
}

// 刷新绑定账号信息
export function refreshInformation(params) {
  return request({
    url: '/inquirySet/refreshInformation',
    method: 'get',
    params
  })
}

// 获取绑定账号信息
export function getTingInformation(params) {
  return request({
    url: '/inquirySet/getTingInformation',
    method: 'get',
    params
  })
}

// 套餐购买
export function packagePurchase(data) {
  return request({
    url: '/inquire/manage/packagePurchase',
    method: 'post',
    data
  })
}

// 查询微信支付结果接口
export function wechatPay(params) {
  return request({
    url: '/inquire/manage/wechatPay',
    method: 'get',
    params
  })
}

// 获取草稿箱详情
export function getDraftBoxDetails(params) {
  return request({
    url: 'inquire/manage/getDraftBoxDetails',
    method: 'get',
    params
  })
}

// 获取草稿箱列表
export function getInquireDraftBoxList(data) {
  return request({
    url: '/inquire/manage/getInquireDraftBoxList',
    method: 'post',
    data
  })
}

// 获取联系人信息
export function getContacts() {
  return request({
    url: '/inquire/manage/getContacts',
    method: 'post'
  })
}

// 确认询价清单
export function confirmation(data) {
  return request({
    url: '/inquire/manage/confirmation',
    method: 'post',
    data
  })
}

// 确认询价清单(扣费信息展示)
export function deduction(data) {
  return request({
    url: '/inquire/manage/deduction',
    method: 'post',
    data
  })
}

// 获取询价项目列表
export function selectInquiryItems(params) {
  return request({
    url: '/inquire/manage/selectInquiryItems',
    method: 'post',
    params
  })
}

// 添加询价项目
export function addInquiryItem(params) {
  return request({
    url: '/inquire/manage/addInquiryItem',
    method: 'post',
    params
  })
}

// 校验项目名称
export function getInquiryItem(params) {
  return request({
    url: '/inquire/manage/getInquiryItem',
    method: 'post',
    params
  })
}

// 清单/附件询价附件下载
export function inquiryDownloadAnnex(params) {
  return request({
    url: '/inquire/manage/inquiryDownloadAnnex',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 创建订单(通宝支付)
export function submitOrders(data) {
  return request({
    url: '/inquire/manage/submitOrders',
    method: 'post',
    data
  })
}

// 取消订单
export function cancelOrder(params) {
  return request({
    url: '/inquirySet/cancelOrder',
    method: 'get',
    params
  })
}

// 查询报价单列表
export function queryQuotationList(params) {
  return request({
    url: '/inquire/manage/queryQuotationList',
    method: 'post',
    params
  })
}

// 报价单下载
export function quotationDownloadAnnex(params) {
  return request({
    url: '/inquire/manage/quotationDownloadAnnex',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 获取账号使用的发票数据
export function getInvoice(params) {
  return request({
    url: '/inquire/manage/getInvoice',
    method: 'get',
    params
  })
}

// 数据采集批量询价跳转回显材料数据
export function dataCollectionBulkInquiry(params) {
  return request({
    url: '/complex/dataCollectionBulkInquiry',
    method: 'get',
    params
  })
}
