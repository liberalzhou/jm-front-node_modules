import request from '@/utils/request'

// 获取询价审核列表
export function getInquiryAuditList(data) {
    return request({
      url: '/inquiryAudit/getInquiryAuditList',
      method: 'post',
      data
    })
}

// 审核询价批次信息
export function auditInquiry(data) {
    return request({
      url: '/inquiryAudit/auditInquiry',
      method: 'post',
      data
    })
}

// 获取询价库材料列表
export function getInquiryMaterialPageList(data) {
    return request({
      url: '/inquiryAudit/getInquiryMaterialPageList',
      method: 'post',
      data
    })
}

// 根据ID获取批次详情信息
export function getInquiryInfoById(params) {
  return request({
    url: '/inquiryAudit/getInquiryInfoById',
    method: 'post',
    params
  })
}

// 获取造价通工程师信息
export function getEngineerInfo(params) {
  return request({
    url: '/inquire/manage/getEngineerInfo',
    method: 'post',
    params
  })
}

// 根据批次ID获取审核记录
export function getAuditRecordListById(params) {
  return request({
    url: '/inquiryAudit/getAuditRecordListById',
    method: 'post',
    params
  })
}

// 导出询价库材料excel
export function exportInquiryMaterial(data) {
  return request({
    url: '/inquiryAudit/exportInquiryMaterial',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 获取批次流程状态
export function getBatchProcessStatus(params) {
  return request({
    url: '/inquire/manage/getBatchProcessStatus',
    method: 'get',
    params
  })
}

// 确认询价完成扣费信息展示
export function deductionInformation(data) {
  return request({
    url: '/inquire/manage/deductionInformation',
    method: 'post',
    data
  })
}

// 确认完成
export function confirmComplete(data) {
  return request({
    url: '/inquire/manage/confirmComplete',
    method: 'post',
    data
  })
}

// 询价批次评价
export function evaluate(data) {
  return request({
    url: '/inquire/manage/evaluate',
    method: 'post',
    data
  })
}

// 获取评价印象标签列表
export function commentTag(params) {
  return request({
    url: '/inquire/manage/CommentTag',
    method: 'post',
    params
  })
}

// 批次报价追问
export function batchQuotationInquiry(params) {
  return request({
    url: '/inquire/manage/batchQuotationInquiry',
    method: 'post',
    params
  })
}

// 查询批次报价追问信息
export function quotationInquiryInformation(params) {
  return request({
    url: '/inquire/manage/quotationInquiryInformation',
    method: 'post',
    params
  })
}