import request from '@/utils/request'

// 获取指标树状图
export function getIndicatorTree(data) {
  return request({
    url: '/indicator/project/getIndicatorTree',
    method: 'post',
    data
  })
}

// 获取指标列表
export function getEconListByStageId(data) {
  return request({
    url: '/indicator/project/getEconListByStageId',
    method: 'post',
    data
  })
}

// 通过指标id获取指标详情
export function getDetailsIndicators(params) {
  return request({
    url: '/indicator/project/getDetailsIndicators',
    method: 'post',
    params
  })
}

// 设置无效/有效指标
export function setIndicator(params) {
  return request({
    url: '/indicator/project/setIndicator',
    method: 'post',
    params
  })
}

// 获取指标走势
export function trendIndicator(data) {
  return request({
    url: '/indicator/project/trendIndicator',
    method: 'post',
    data
  })
}

// 导出指标
export function exportIndicator(data) {
  return request({
    url: '/indicator/project/exportIndicator',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 体检报告
export function medicalReport(data) {
  return request({
    url: '/project/base/medicalReport',
    method: 'post',
    data
  })
}

// 信息检测
export function informationDetection(data) {
  return request({
    url: '/project/base/informationDetection',
    method: 'post',
    data
  })
}

// 指标预警
export function indicatorWarning(data) {
  return request({
    url: '/project/base/indicatorWarning',
    method: 'post',
    data
  })
}

// 数据检查
export function dataCheck(data) {
  return request({
    url: '/project/base/dataCheck',
    method: 'post',
    data
  })
}

// 下载报告
export function downloadReport(data) {
  return request({
    url: '/project/base/downloadReport',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 添加指标预警备注
export function indicatorWarningNotes(params) {
  return request({
    url: '/project/base/indicatorWarningNotes',
    method: 'post',
    params
  })
}

// 可视化指标预警
export function visualIndicatorWarning(params) {
  return request({
    url: '/project/detail/visualIndicatorWarning',
    method: 'post',
    params
  })
}

// 2.1
// 单项工程工程概况对比-列表
export function getProjectCompareOverview(data) {
  return request({
    url: '/project/engineering/getProjectCompareOverview',
    method: 'post',
    data
  })
}

// 单项工程单位工程对比-列表
export function getProjectCompareUnitEng(data) {
  return request({
    url: '/project/engineering/getProjectCompareUnitEng',
    method: 'post',
    data
  })
}

// 单项工程分部分项对比-列表
export function getProjectCompareDept(data) {
  return request({
    url: '/project/engineering/getProjectCompareDept',
    method: 'post',
    data
  })
}

// 单项工程工料机对比-列表
export function getProjectCompareMat(data) {
  return request({
    url: '/project/engineering/getProjectCompareMat',
    method: 'post',
    data
  })
}

// 单项工程工程全过程概况对比-列表
export function comparisonProcessOverView(params) {
  return request({
    url: '/project/engineering/comparisonProcessOverView',
    method: 'post',
    params
  })
}

// 单项工程工程全过程单位工程对比-列表
export function comparisonProcessUnitEng(params) {
  return request({
    url: '/project/engineering/comparisonProcessUnitEng',
    method: 'post',
    params
  })
}

// 单项工程工程全过程分部分项对比-列表
export function comparisonProcessDept(params) {
  return request({
    url: '/project/engineering/comparisonProcessDept',
    method: 'post',
    params
  })
}

// 单项工程工程全过程工料机对比-列表
export function comparisonProcessMat(params) {
  return request({
    url: '/project/engineering/comparisonProcessMat',
    method: 'post',
    params
  })
}

// 项目对比下拉框数据
export function getProjectDropDownBox(data) {
  return request({
    url: '/project/engineering/getProjectDropDownBox',
    method: 'post',
    data
  })
}

// 项目全过程对比下拉框数据
export function processDropDownBox(params) {
  return request({
    url: '/project/engineering/processDropDownBox',
    method: 'post',
    params
  })
}

// 项目全过程对比-图表
export function comparisonProcessView(data) {
  return request({
    url: '/project/engineering/comparisonProcessView',
    method: 'post',
    data
  })
}

// 项目全过程对比-列表
export function comparisonProcessProjectList(params) {
  return request({
    url: '/project/engineering/comparisonProcessProjectList',
    method: 'post',
    params
  })
}

// 项目对比-列表
export function projectComparisonProcessList(data) {
  return request({
    url: '/project/engineering/projectComparisonProcessList',
    method: 'post',
    data
  })
}

// 项目对比-图表
export function projectComparisonView(data) {
  return request({
    url: '/project/engineering/projectComparisonView',
    method: 'post',
    data
  })
}

// 判断是否可以全过程对比
export function judgeProcessCompared(params) {
  return request({
    url: '/project/engineering/judgeProcessCompared',
    method: 'post',
    params
  })
}

// 大项目对比导出
export function exportProjectComparison(data) {
  return request({
    url: '/project/engineering/exportProjectComparison',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 大项目全过程对比导出
export function exportProcessCompared(params) {
  return request({
    url: '/project/engineering/exportProcessCompared',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 单项工程对比导出
export function exportSingleProjectComparison(data) {
  return request({
    url: '/project/engineering/exportSingleProjectComparison',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 单项工程全过程对比导出
export function exportSingleProjectCompared(params) {
  return request({
    url: '/project/engineering/exportSingleProjectCompared',
    method: 'post',
    params,
    responseType: 'blob'
  })
}
