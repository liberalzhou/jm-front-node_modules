import request from '@/utils/request'

// 查询已有材价的适用时间
export function getMatApplicableTime() {
  return request({
    url: '/mat/matprice/getMatApplicableTime',
    method: 'post'
  })
}

// 获取材价国标分类列表
export function getMatSubClassify(data) {
  return request({
    url: '/mat/matprice/getMatSubClassify',
    method: 'post',
    data
  })
}

// 获取材价列表
export function getMatPricePageList(data) {
  return request({
    url: '/mat/matprice/getMatPricePageList',
    method: 'post',
    data
  })
}

// 根据条件查询材价名称列表
export function getMaterialNameList(data) {
  return request({
    url: '/mat/matprice/getMaterialNameList',
    method: 'post',
    data
  })
}

// 删除材价
export function delMatPrice(params) {
  return request({
    url: '/mat/matprice/delMatPrice',
    method: 'post',
    params
  })
}

// 重新标准化
export function againFormat(params) {
  return request({
    url: '/mat/matprice/againFormat',
    method: 'post',
    params
  })
}

// 导出材价信息excel
export function exportMatPrice(data) {
  return request({
    url: '/mat/matprice/exportMatPrice',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 编辑材价信息
export function updateMatPrice(data) {
  return request({
    url: '/mat/matprice/updateMatPrice',
    method: 'post',
    data
  })
}

// 导入材价文件
export function importMatPrice(data) {
  return request({
    url: '/mat/matprice/importMatPrice',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 下载材价导入模板
export function getMatImportTemplate() {
  return request({
    url: '/mat/matprice/getMatImportTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

// 通过标准化失败的数据key保存标准化失败材价
export function addFailMaterialPrice(data) {
  return request({
    url: '/mat/matprice/addFailMaterialPrice',
    method: 'post',
    data
  })
}

// 添加单条材价
export function addMaterialPrice(data) {
  return request({
    url: '/mat/matprice/addMaterialPrice',
    method: 'post',
    data
  })
}

// 调整分类
export function adjustClassificatione(data) {
  return request({
    url: '/mat/matprice/adjustClassification',
    method: 'post',
    data
  })
}

// 造价通查价
// 单条查价造价通标准化数据
export function singleStandardMatSearchPrice(params) {
  return request({
    url: '/mat/zjtPriceSearch/singleStandardMatSearchPrice',
    method: 'post',
    params
  })
}

// 单条查询造价通列表
export function queryFacMatsList(params) {
  return request({
    url: '/mat/zjtPriceSearch/queryFacMatsList',
    method: 'post',
    params
  })
}

// 下载造价通查询材价模板
export function getMatsSearchTemplate() {
  return request({
    url: '/mat/zjtPriceSearch/getMatsSearchTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

// 多条查询造价通标准化数据 - 批量标准化
export function standardMatSearchPrice(data) {
  return request({
    url: '/mat/zjtPriceSearch/standardMatSearchPrice',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 多条查询造价通材价
export function matSearchPrice(data) {
  return request({
    url: '/mat/zjtPriceSearch/matSearchPrice',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 获取材价通查价历史列表
export function getHistoryPageList(data) {
  return request({
    url: '/mat/zjtPriceSearch/getMatSearchPriceHistoryPageList',
    method: 'post',
    data
  })
}

// 获取材价通查价结果列表
export function getResultPageList(data) {
  return request({
    url: '/mat/zjtPriceSearch/getMatSearchPriceResultPageList',
    method: 'post',
    data
  })
}

// 导出材价查询结果信息excel
export function exportMatPriceResult(data) {
  return request({
    url: '/mat/zjtPriceSearch/exportMatPriceSearchResult',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 多条查询造价通材价下载问题清单
export function matSearchPriceProblemList(data) {
  return request({
    url: '/mat/zjtPriceSearch/matSearchPriceProblemList',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 获取上传材价库的图片
export function getMatPriceImg(data) {
  return request({
    url: '/mat/matprice/getMatPriceImg',
    method: 'post',
    data
  })
}

// 上传材价库的图片
export function uploadMatPriceImg(data) {
  return request({
    url: '/mat/matprice/uploadMatPriceImg',
    method: 'post',
    data
  })
}

// 删除上传材价库的图片
export function deleteMatPriceImg(data) {
  return request({
    url: '/mat/matprice/deleteMatPriceImg',
    method: 'post',
    data
  })
}
// 相同材价->获取相关地区
export function getMatPriceArea(data) {
  return request({
    url: '/mat/matprice/getMatPriceArea',
    method: 'post',
    data
  })
}

