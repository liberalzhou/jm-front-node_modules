import request from '@/utils/request'

// 获取材价列表
export function getFilePageList(data) {
    return request({
        url: '/file/getFilePageList',
        method: 'post',
        data
    })
}

// 新建文件夹
export function addOrUpdateFolder(data) {
    return request({
        url: '/file/addOrUpdateFolder',
        method: 'post',
        data
    })
}

// 查询文档库文件类型
export function getFileTypeList(data) {
    return request({
        url: '/file/getFileTypeList',
        method: 'post',
        data
    })
}

// 删除文件或文件夹
export function deleteFileOrFolder(params) {
    return request({
        url: '/file/deleteFileOrFolder',
        method: 'post',
        params
    })
}

// 下载文件
export function downloadFile(params) {
    return request({
        url: '/file/downloadFile',
        method: 'get',
        responseType: 'blob',
        headers: {
            'Content-Type': 'application/octet-stream',
        },
        params
    })
}

// 上传文件
export function uploadFile(data) {
    return request({
        url: '/file/uploadFile',
        method: 'post',
        data,
        headers: {
            'Content-Type': 'multipart/form-data',
        }
    })
}