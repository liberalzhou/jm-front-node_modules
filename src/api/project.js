
import request from '@/utils/request'

// 查询造价阶段列表
export function queryStageList() {
  return request({
    url: '/project/config/queryStageList',
    method: 'post'
  })
}

// 新增造价阶段
export function addStageConfig(data) {
  return request({
    url: '/project/config/addStageConfig',
    method: 'post',
    data
  })
}

// 编辑造价阶段
export function updateStageConfig(data) {
  return request({
    url: '/project/config/updateStageConfig',
    method: 'post',
    data
  })
}

// 删除造价阶段
export function delStageConfig(params) {
  return request({
    url: '/project/config/delStageConfig',
    method: 'post',
    params
  })
}

// 造价阶段是否启用自定义名称配置
export function stageCustomSwitchConfig(params) {
  return request({
    url: '/project/config/stageCustomSwitchConfig',
    method: 'post',
    params
  })
}

// 工程配置是否启用自定义名称配置
export function configCustomName(params) {
  return request({
    url: '/project/config/configCustomName',
    method: 'post',
    params
  })
}

// 查询工程配置列表
export function queryNormEngList(data) {
  return request({
    url: '/project/config/queryNormEngList',
    method: 'post',
    data
  })
}

// 新增工程配置
export function addNormEngConfig(data) {
  return request({
    url: '/project/config/addNormEngConfig',
    method: 'post',
    data
  })
}

// 编辑工程配置
export function updateEngConfig(data) {
  return request({
    url: '/project/config/updateEngConfig',
    method: 'post',
    data
  })
}

// 删除工程配置
export function delNormEngConfig(params) {
  return request({
    url: '/project/config/delNormEngConfig',
    method: 'post',
    params
  })
}

// 新增技术特征配置
export function addTecFeatureConfig(data) {
  return request({
    url: '/project/config/addTecFeatureConfig',
    method: 'post',
    data
  })
}

// 查询技术特征配置列表
export function queryTecFeatureList(data) {
  return request({
    url: '/project/config/queryTecFeatureList',
    method: 'post',
    data
  })
}

// 查询项目类型树状列表
export function queryProjectTypeTree(params) {
  return request({
    url: '/project/config/queryProjectTypeTree',
    method: 'post',
    params
  })
}

// 删除技术特征配置
export function delTecFeatureConfig(params) {
  return request({
    url: '/project/config/delTecFeatureConfig',
    method: 'post',
    params
  })
}

// 启用或禁用技术特征配置
export function tecFeatureSwitchConfig(params) {
  return request({
    url: '/project/config/tecFeatureSwitchConfig',
    method: 'post',
    params
  })
}

// 项目对比详情
export function getProjectCompareView(data) {
  return request({
    url: '/project/detail/getProjectCompareView',
    method: 'post',
    data
  })
}

// 阶段对比详情
export function getProjectStageView(data) {
  return request({
    url: '/project/detail/getProjectStageView',
    method: 'post',
    data
  })
}

// 项目对比下拉框数据
export function getProjectUnitItemList(data) {
  return request({
    url: '/project/detail/getProjectUnitItemList',
    method: 'post',
    data
  })
}

// 获取项目列表
export function getProjectPageList(data) {
  return request({
    url: '/project/detail/getProjectPageList',
    method: 'post',
    data
  })
}

// 获取项目阶段列表
export function getProjectStageList(data) {
  return request({
    url: '/project/detail/getProjectStageList',
    method: 'post',
    data
  })
}

// 项目库可视化-项目工程造价总图表
export function getProjectEngView(params) {
  return request({
    url: '/project/detail/getProjectEngView',
    method: 'post',
    params
  })
}

// 项目库可视化-各单位工程费用明细表
export function getProjectEngDetailPriceView(params) {
  return request({
    url: '/project/detail/getProjectEngDetailPriceView',
    method: 'post',
    params
  })
}

// 项目库可视化-工料机费用及比例图表
export function getProjectMatView(params) {
  return request({
    url: '/project/detail/getProjectMatView',
    method: 'post',
    params
  })
}

// 查询项目导入文件单位工程列表
export function getProjectEngList(params) {
  return request({
    url: '/project/base/getProjectEngList',
    method: 'post',
    params
  })
}

// 获取项目阶段详细
export function getProStageInfo(params) {
  return request({
    url: '/project/detail/getProStageInfo',
    method: 'post',
    params
  })
}

// 获取项目基础信息z
export function getProjectBaseInfo(params) {
  return request({
    url: '/project/detail/getProjectBaseInfo',
    method: 'post',
    params
  })
}

// 查询可配置项目特征列表
export function getTecFeatureByProject(params) {
  return request({
    url: '/project/base/getTecFeatureByProject',
    method: 'post',
    params
  })
}

// 项目库可视化-各分项清单工程费用明细表
export function getProjectItemInventoryDetailPriceView(data) {
  return request({
    url: '/project/detail/getProjectItemInventoryDetailPriceView',
    method: 'post',
    data
  })
}

// 各分部分项工程费用明细表
export function getProjectDeptItemDetailPriceView(params) {
  return request({
    url: '/project/detail/getProjectDeptItemDetailPriceView',
    method: 'post',
    params
  })
}

// 查询项目清单一览表
export function getProjectDetailTable(data) {
  return request({
    url: '/project/eng/getProjectDetailTable',
    method: 'post',
    data
  })
}

// 查询项目清单一览表-定额
export function getProjectDetailQuota(params) {
  return request({
    url: '/project/eng/getProjectDetailQuota',
    method: 'post',
    params
  })
}

// 查询工料机汇总表
export function getProjectMatCount(params) {
  return request({
    url: '/project/eng/getProjectMatCount',
    method: 'post',
    params
  })
}

// 查询工料机汇总详情
export function getProjectMatCountDetail(data) {
  return request({
    url: '/project/eng/getProjectMatCountDetail',
    method: 'post',
    data
  })
}

// 查询分部汇总
export function getUnitProjectCount(params) {
  return request({
    url: '/project/eng/getUnitProjectCount',
    method: 'post',
    params
  })
}

// 导出项目基础信息
export function exportProjectBaseInfo(params) {
  return request({
    url: '/project/detail/exportProjectBaseInfo',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 工料机汇总表导出
export function exportProjectMat(params) {
  return request({
    url: '/project/eng/exportProjectMat',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 查询工料机汇总详情导出
export function exportMatCountDetail(data) {
  return request({
    url: '/project/eng/exportMatCountDetail',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 分部分项清单工程费用导出
export function exportProjectItemInventoryDetailPriceView(params) {
  return request({
    url: '/project/detail/exportProjectItemInventoryDetailPriceView',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 清单定额导出
export function exportProjectDetailTable(data) {
  return request({
    url: '/project/eng/exportProjectDetailTable',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 各单位工程费用明细导出
export function exporttEngDetailPrice(params) {
  return request({
    url: '/project/detail/exporttEngDetailPrice',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 估算费用明细导出
export function exportEstInfo(params) {
  return request({
    url: '/project/detail/exportEstInfo',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// （项目列表）项目导出
export function exportProjectPageList(data) {
  return request({
    url: '/project/detail/exportProjectPageList',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 判断造价阶段是否存在数据
export function getDelStageConfig(params) {
  return request({
    url: '/project/config/getDelStageConfig',
    method: 'post',
    params
  })
}

// 根据项目阶段获取项目修改日志
export function getProjectLog(params) {
  return request({
    url: '/project/eng/getProjectLog',
    method: 'post',
    params
  })
}

