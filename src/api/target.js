import request from '@/utils/request'

// (指标库)
// 获取指标列表
export function getMetricsList(data) {
  return request({
    url: '/indicator/base/getMetricsList',
    method: 'post',
    data
  })
}

// 获取工程类型树状图
export function getProjectTypeTree(data) {
  return request({
    url: '/indicator/base/getProjectTypeTree',
    method: 'post',
    data
  })
}

// 设置无效/有效指标
export function setIndicator(params) {
  return request({
    url: '/indicator/project/setIndicator',
    method: 'post',
    params
  })
}

// 导出指标
export function exportIndicator(data) {
  return request({
    url: '/indicator/base/exportIndicator',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 指标计算
export function indicatorCalculation(data) {
  return request({
    url: '/indicator/base/indicatorCalculation',
    method: 'post',
    data
  })
}

// 提交至综合指标库
export function submitIndicatorPlural(data) {
  return request({
    url: '/indicator/base/submitIndicatorPlural',
    method: 'post',
    data
  })
}

// 指标分析
export function indicatorAnalysis(data) {
  return request({
    url: '/indicator/base/indicatorAnalysis',
    method: 'post',
    data
  })
}

// 综合指标 - 获取综合指标列表
export function getPluralMetricsList(data) {
  return request({
    url: '/indicator/plural/getPluralMetricsList',
    method: 'post',
    data
  })
}

// 综合指标 - 获取综合指标详情
export function pluralIndicatorDetails(params) {
  return request({
    url: '/indicator/plural/pluralIndicatorDetails',
    method: 'post',
    params
  })
}

// 综合指标 - 删除指标
export function deletePuralIndicator(data) {
  return request({
    url: '/indicator/plural/deleteIndicator',
    method: 'post',
    data
  })
}

// 综合指标 - 导出指标
export function exportPuralIndicator(data) {
  return request({
    url: '/indicator/plural/exportIndicator',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 综合指标 - 获取综合指标换算时间树
export function conversionTimeTree(params) {
  return request({
    url: '/indicator/plural/conversionTimeTree',
    method: 'post',
    params
  })
}

// 综合指标审核 - 审核综合指标
export function verifyPlural(data) {
  return request({
    url: '/indicator/plural/verifyPlural',
    method: 'post',
    data
  })
}

// 综合指标审核 - 获取提交员列表
export function getAListOfCommitters(params) {
  return request({
    url: '/indicator/plural/getAListOfCommitters',
    method: 'post',
    params
  })
}

// (新项目智能应用)
// 快速造价测算数据列表
export function listPage(data) {
  return request({
    url: '/indicator/costcalculate/listPage',
    method: 'post',
    data
  })
}

// 删除快速造价测算
export function deleteApi(params) {
  return request({
    url: '/indicator/costcalculate/delete',
    method: 'post',
    params
  })
}

// 获取测算方案
export function getIndicatorEconConfig(data) {
  return request({
    url: '/indicator/costcalculate/getIndicatorEconConfig',
    method: 'post',
    data
  })
}

// 获取综合指标库列表
export function getCostPluralMetricsList(data) {
  return request({
    url: '/indicator/costcalculate/getCostPluralMetricsList',
    method: 'post',
    data
  })
}

// 获取指标样本数据(略)
export function getSampleList(data) {
  return request({
    url: '/indicator/costcalculate/getSampleList',
    method: 'post',
    data
  })
}

// 指标计算
export function calculation(data) {
  return request({
    url: '/indicator/costcalculate/calculation',
    method: 'post',
    data
  })
}

// 指标重新计算
export function againCalculation(data) {
  return request({
    url: '/indicator/costcalculate/againCalculation',
    method: 'post',
    data
  })
}

// 添加快速造价测算数据
export function saveApi(data) {
  return request({
    url: '/indicator/costcalculate/save',
    method: 'post',
    data
  })
}

// 修改快速造价测算数据
export function updateApi(data) {
  return request({
    url: '/indicator/costcalculate/update',
    method: 'post',
    data
  })
}

// 查看快速造价测算详细数据
export function infoApi(params) {
  return request({
    url: '/indicator/costcalculate/info',
    method: 'post',
    params
  })
}

// 获取综合占比
export function getMakeCalculate(data) {
  return request({
    url: '/indicator/costcalculate/getMakeCalculate',
    method: 'post',
    data
  })
}

// 获取换算时间后的单方造价
export function getConversionCalculate(data) {
  return request({
    url: '/indicator/costcalculate/getConversionCalculate',
    method: 'post',
    data
  })
}

// (指标参考)
// 常用综合指标实时参考列表
export function consultListPage(data) {
  return request({
    url: '/indicator/consult/listPage',
    method: 'post',
    data
  })
}

// 下载指标
export function consultExport(data) {
  return request({
    url: '/indicator/consult/export',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 已选特征详情
export function featureInfo(params) {
  return request({
    url: '/indicator/consult/featureInfo',
    method: 'post',
    params
  })
}

// 指标计算
export function consultCalculation(data) {
  return request({
    url: '/indicator/consult/calculation',
    method: 'post',
    data
  })
}

// 指标重新计算
export function consultAgainCalculation(data) {
  return request({
    url: '/indicator/consult/againCalculation',
    method: 'post',
    data
  })
}

// (快速检测)
// 校验建筑业态下是否存在指标预警方案
export function presenceWarning(data) {
  return request({
    url: '/project/indicator/presenceWarning',
    method: 'post',
    data
  })
}

// 创建或更新检测项目和项目阶段
export function createOrUpdateDetectProject(data) {
  return request({
    url: '/project/indicator/createOrUpdateDetectProject',
    method: 'post',
    data
  })
}

// 导入检测文件
export function importDetectFile(data) {
  return request({
    url: '/project/indicator/importDetectFile',
    method: 'post',
    data
  })
}

// 删除项目信息
export function delProjectInfo(data) {
  return request({
    url: '/project/base/delProjectInfo',
    method: 'post',
    data
  })
}

// 快速检测预警指标
export function quickCheck(data) {
  return request({
    url: '/project/indicator/quickCheck',
    method: 'post',
    data
  })
}

// 下载报告
export function downloadReport(data) {
  return request({
    url: '/project/indicator/downloadReport',
    method: 'post',
    data,
    responseType: 'blob'
  })
}
// 获取指标详情（根据指标id计算获取）
export function indicatorIdsPlural(data) {
  return request({
    url: '/indicator/base/indicatorIdsPlural',
    method: 'post',
    data
  })
}

// (大项目-快速造价测算)
// 快速造价测算数据列表
export function bigListPage(data) {
  return request({
    url: '/indicator/costcalculate/project/listPage',
    method: 'post',
    data
  })
}

// 删除快速造价测算
export function deleteBig(params) {
  return request({
    url: '/indicator/costcalculate/project/delete',
    method: 'post',
    params
  })
}

// 获取综合指标库列表
export function getBigCostPluralMetricsList(data) {
  return request({
    url: '/indicator/costcalculate/project/getCostPluralMetricsList',
    method: 'post',
    data
  })
}

// 查看快速造价测算详细数据
export function bigInfo(params) {
  return request({
    url: '/indicator/costcalculate/project/info',
    method: 'post',
    params
  })
}

// 添加快速造价测算数据
export function bigSave(data) {
  return request({
    url: '/indicator/costcalculate/project/save',
    method: 'post',
    data
  })
}

// 修改快速造价测算数据
export function bigUpdate(data) {
  return request({
    url: '/indicator/costcalculate/project/update',
    method: 'post',
    data
  })
}

// 获取换算时间后的单方造价
export function getBigConversionCalculate(data) {
  return request({
    url: '/indicator/costcalculate/project/getConversionCalculate',
    method: 'post',
    data
  })
}

// 指标计算
export function bigValculation(data) {
  return request({
    url: '/indicator/costcalculate/project/calculation',
    method: 'post',
    data
  })
}

// 指标重新计算
export function bigAgainCalculation(data) {
  return request({
    url: '/indicator/costcalculate/project/againCalculation',
    method: 'post',
    data
  })
}

