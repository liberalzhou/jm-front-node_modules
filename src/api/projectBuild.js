import request from '@/utils/request'

// 获取单项工程分类
export function getProjectType(data) {
  return request({
    url: '/matrix/build/getProjectType',
    method: 'post',
    data
  })
}

// 获取单位(分部)工程分类
export function getUnitProjectType(data) {
  return request({
    url: '/matrix/build/getUnitProjectType',
    method: 'post',
    data
  })
}

// 选择分部工程
export function choiceUnitProject(data) {
  return request({
    url: '/matrix/build/choiceUnitProject',
    method: 'post',
    data
  })
}

// 查看分部工程详情
export function lookUnitProjectInfo(params) {
  return request({
    url: '/matrix/build/lookUnitProjectInfo',
    method: 'post',
    params
  })
}

// 查看分部工程详情再详情(各分项清单工程费用明细表)
export function lookDeptProjectInfo(data) {
  return request({
    url: '/matrix/build/lookDeptProjectInfo',
    method: 'post',
    data
  })
}

// 获取单位工程换算造价金额
export function getMatrixUnitPrice(data) {
  return request({
    url: '/matrix/build/getMatrixUnitPrice',
    method: 'post',
    data
  })
}

// 查看单位工程换算
export function lookMatrixUnitProject(data) {
  return request({
    url: '/matrix/build/lookMatrixUnitProject',
    method: 'post',
    data
  })
}

// 获取换算材价的更多价格
export function getMoreOtherMatPrice(data) {
  return request({
    url: '/matrix/build/getMoreOtherMatPrice',
    method: 'post',
    data
  })
}

// 保存项目自由构建换算
export function saveProjectBuild(data) {
  return request({
    url: '/matrix/build/saveProjectBuild',
    method: 'post',
    data
  })
}

// 新增项目自由构建换算
export function addProjectBuild(data) {
  return request({
    url: '/matrix/build/history/addProjectBuild',
    method: 'post',
    data
  })
}

// 获取换算详情数据
export function getMatrixProjectOnUnit(params) {
  return request({
    url: '/matrix/build/history/getMatrixProjectOnUnit',
    method: 'post',
    params
  })
}

// 构建历史页面============================================
// 换算历史列表
export function getMatrixBuildHistoryPage(data) {
  return request({
    url: '/matrix/build/history/getMatrixBuildHistoryPage',
    method: 'post',
    data
  })
}

// 删除
export function delMatrixBuildHistory(params) {
  return request({
    url: '/matrix/build/history/delMatrixBuildHistory',
    method: 'post',
    params
  })
}

// 导出工程换算
export function exportMatrixBuildProject(data) {
  return request({
    url: '/matrix/build/history/exportMatrixBuildProject',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 获取同类工程换算
export function getAlikeMatrixBuildHistory(params) {
  return request({
    url: '/matrix/build/history/getAlikeMatrixBuildHistory',
    method: 'post',
    params
  })
}

