
import request from '@/utils/request'

// 创建项目-----------------------------------------------------------------

// 查询项目类型树状列表
export function queryProjectTypeTree(data) {
  return request({
    url: '/project/config/queryProjectTypeTree',
    method: 'post',
    data
  })
}

// 查询技术特征配置列表
export function queryTecFeatureList(data) {
  return request({
    url: '/project/config/queryTecFeatureList',
    method: 'post',
    data
  })
}

// 校验项目名称是否存在
export function validateProjectName(data) {
  return request({
    url: '/project/base/validateProjectName',
    method: 'post',
    data
  })
}

// 创建或更新项目
export function createOrUpdateProject(data) {
  return request({
    url: '/project/base/createOrUpdateProject',
    method: 'post',
    data
  })
}

// 新增或更新技术特征
export function addOrUpdateTecFeature(data) {
  return request({
    url: '/project/base/addOrUpdateTecFeature',
    method: 'post',
    data
  })
}

//  导入文件----------------------------------------------------------------

// 查询项目可选造价阶段(供导入新阶段时查询阶段)
export function getStageListByProject(params) {
  return request({
    url: '/project/base/getStageListByProject',
    method: 'post',
    params
  })
}

// 查询项目导入文件单位工程列表
export function getProjectStageEngList(data) {
  return request({
    url: '/project/base/getProjectStageEngList',
    method: 'post',
    data
  })
}

// 新增或更新项目阶段信息
export function addOrUpdateProStage(data) {
  return request({
    url: '/project/base/addOrUpdateProStage',
    method: 'post',
    data
  })
}

// 新增或更新项目阶段信息(完成标准化校验时)
export function setProjectCategories(data) {
  return request({
    url: '/project/base/setProjectCategories',
    method: 'post',
    data
  })
}

// 导入项目清单文件
export function importProjectFile(data) {
  return request({
    url: '/project/file/importProjectFile',
    method: 'post',
    data
  })
}

// 导入总概算文件
export function importTotalEstimateFile(data) {
  return request({
    url: '/project/file/importTotalEstimateFile',
    method: 'post',
    data
  })
}

// 导入项目清单文件(估算)
export function getEstLists(data) {
  return request({
    url: '/project/eng/getEstLists',
    method: 'post',
    data
  })
}

// 根据工程id、已匹配的项目id集合和造价阶段id查询工程已有项目
export function queryExistingSingleProject(params) {
  return request({
    url: '/project/eng/queryExistingSingleProject',
    method: 'post',
    params
  })
}

// 根匹配单项工程
export function matchProject(data) {
  return request({
    url: '/project/eng/matchProject',
    method: 'post',
    data
  })
}

// 查询造价阶段列表
export function queryStageList(data) {
  return request({
    url: '/project/config/queryStageList',
    method: 'post',
    data
  })
}

// 根据项目阶段获取单位工程列表
export function getProjectUnitEngList(params) {
  return request({
    url: '/project/eng/getProjectUnitEngList',
    method: 'post',
    params
  })
}

// 根据项目单位工程获取清单列表
export function getInventoryList(data) {
  return request({
    url: '/project/eng/getInventoryList',
    method: 'post',
    data
  })
}

// 根据类型查询工程及下级分类信息
export function queryNormList(data) {
  return request({
    url: '/project/config/queryNormList',
    method: 'post',
    data
  })
}

// 修改清单所属工程
export function updateInvAscriptions(data) {
  return request({
    url: '/project/eng/updateInvAscriptions',
    method: 'post',
    data
  })
}

// 修改清单信息
export function updateInvInfo(data) {
  return request({
    url: '/project/eng/updateInvInfo',
    method: 'post',
    data
  })
}

// 修改定额信息
export function updateInvQuotaInfo(data) {
  return request({
    url: '/project/eng/updateInvQuotaInfo',
    method: 'post',
    data
  })
}

// 删除清单信息
export function delInvInfo(data) {
  return request({
    url: '/project/eng/delInvInfo',
    method: 'post',
    data
  })
}

// 查询估算信息列表
export function getEstList(data) {
  return request({
    url: '/project/eng/getEstList',
    method: 'post',
    data
  })
}

// 修改估算信息
export function updateEstInfo(data) {
  return request({
    url: '/project/eng/updateEstInfo',
    method: 'post',
    data
  })
}

// 删除估算信息
export function delEstInfo(data) {
  return request({
    url: '/project/eng/delEstInfo',
    method: 'post',
    data
  })
}

// 完成标准化校验
export function validateNormal(data) {
  return request({
    url: '/project/base/validateNormal',
    method: 'post',
    data
  })
}

// 根据项目阶段id集合获取单位工程列表
export function getProjectUnitEngListByStageIds(data) {
  return request({
    url: '/project/eng/getProjectUnitEngListByStageIds',
    method: 'post',
    data
  })
}

// 通过工程阶段id获取信息
export function getInformationByProjectStageId(params) {
  return request({
    url: '/project/base/getInformationByProjectStageId',
    method: 'post',
    params
  })
}

// 通过项目阶段id获取信息
export function getDataByProjectStageId(params) {
  return request({
    url: '/project/base/getDataByProjectStageId',
    method: 'post',
    params
  })
}

// 项目管理----------------------------------------------------------------

// 获取项目管理列表
export function getProjectPageList(data) {
  return request({
    url: '/project/detail/getProjectPageList',
    method: 'post',
    data
  })
}

// 设置项目典型
export function configTypical(data) {
  return request({
    url: '/project/base/configTypical',
    method: 'post',
    data
  })
}

// 删除项目
export function delProjectInfo(data) {
  return request({
    url: '/project/base/delProjectInfo',
    method: 'post',
    data
  })
}

// 获取项目阶段列表
export function getProjectStageList(data) {
  return request({
    url: '/project/detail/getProjectStageList',
    method: 'post',
    data
  })
}

// 获取所有项目取定期区间
export function getProjectTimeRange() {
  return request({
    url: '/project/detail/getProjectTimeRange',
    method: 'post'
  })
}

// 新增或更新建设其他费
export function addOrUpdateOtherCost(data) {
  return request({
    url: '/project/base/addOrUpdateOtherCost',
    method: 'post',
    data
  })
}

// 删除建设其他费
export function delOtherCost(params) {
  return request({
    url: '/project/base/delOtherCost',
    method: 'post',
    params
  })
}

// 新增或更新项目备注
export function addOrUpdateProRemark(data) {
  return request({
    url: '/project/base/addOrUpdateProRemark',
    method: 'post',
    data
  })
}

// 新增或更新项目备注
export function delProRemark(params) {
  return request({
    url: '/project/base/delProRemark',
    method: 'post',
    params
  })
}

// 阶段对比详情
export function getProjectStageView(data) {
  return request({
    url: '/project/detail/getProjectStageView',
    method: 'post',
    data
  })
}

// 阶段对比详情-切换项目时的列表查询
export function getPhaseCompProList(data) {
  return request({
    url: '/project/detail/getPhaseCompProList',
    method: 'post',
    data
  })
}

// 删除阶段单位工程数据
export function delUnitEngInfo(params) {
  return request({
    url: '/project/eng/delUnitEngInfo',
    method: 'post',
    params
  })
}

// 查询项目已有阶段信息(针对项目中存在已被删除的阶段情况)
export function getOldStageListByProject(params) {
  return request({
    url: '/project/base/getOldStageListByProject',
    method: 'post',
    params
  })
}

// 下载建设其他费上传模板
export function downloadOtherCostTemplate() {
  return request({
    url: '/template/downloadOtherCostTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

// 下载建设其他费上传模板
export function downloadEstTemplate() {
  return request({
    url: '/template/downloadEstTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

// 造价定额标准配置列表
export function getQuotaConfigList(data) {
  return request({
    url: '/project/quota/config/getQuotaConfigList',
    method: 'post',
    data
  })
}

// 根据城市获取造价定额标准配置列表
export function getCityQuotaConfigList(data) {
  return request({
    url: '/project/quota/config/getCityQuotaConfigList',
    method: 'post',
    data
  })
}

// 新增造价定额标准配置
export function addQuotaConfig(data) {
  return request({
    url: '/project/quota/config/addQuotaConfig',
    method: 'post',
    data
  })
}

// 修改造价定额标准配置
export function updateQuotaConfig(data) {
  return request({
    url: '/project/quota/config/updateQuotaConfig',
    method: 'post',
    data
  })
}

// 删除造价定额标准配置
export function delQuotaConfig(params) {
  return request({
    url: '/project/quota/config/delQuotaConfig',
    method: 'post',
    params
  })
}

// 根据项目阶段判断工程异常清单
export function judgmentException(params) {
  return request({
    url: '/project/eng/judgmentException',
    method: 'post',
    params
  })
}

// 根据文件id删除文件数据
export function deleteFileData(data) {
  return request({
    url: '/project/file/deleteFileData',
    method: 'post',
    data
  })
}

// 添加单项工程回显(略)
export function singleProjectEcho(params) {
  return request({
    url: '/project/file/singleProjectEcho',
    method: 'post',
    params
  })
}

// 添加单位工程回显
export function unitProjectEcho(params) {
  return request({
    url: '/project/file/singleProjectSingleProjectEcho',
    method: 'post',
    params
  })
}

// 根据大项目阶段id获取单项工程列表
export function getSingleProjectList(params) {
  return request({
    url: '/project/engineering/getSingleProjectList',
    method: 'post',
    params
  })
}

// 获取总估/概算表(路径,下载用)
export function getSummaryTable(params) {
  return request({
    url: '/project/engineering/getSummaryTable',
    method: 'post',
    params
  })
}

// 获取总估/概算表(文件流)
export function getInputStream(params) {
  return request({
    url: 'project/engineering/getInputStream',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 获取总估/概算表(文件流)
export function getBase64(params) {
  return request({
    url: 'project/engineering/getBase64',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 各单项工程费用明细表
export function getProjectExpenseList(params) {
  return request({
    url: '/project/engineering/getProjectExpenseList',
    method: 'post',
    params
  })
}

// 大项目导出单项工程汇总表
export function exportProjectList(params) {
  return request({
    url: '/project/engineering/exportProjectList',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 项目工料机汇总
export function projectMaterialMachineView(params) {
  return request({
    url: '/project/engineering/projectMaterialMachineView',
    method: 'post',
    params
  })
}

// 大项目导出工料机汇总表
export function exportProjectMatList(params) {
  return request({
    url: '/project/engineering/exportProjectMatList',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 大项目导出成果
export function exportProjectResults(params) {
  return request({
    url: '/project/engineering/exportProjectResults',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 大项目可视化
export function projectVisualization(params) {
  return request({
    url: '/project/engineering/projectVisualization',
    method: 'post',
    params
  })
}

// 判断是否存在总估/概算表
export function determineThere(params) {
  return request({
    url: '/project/engineering/determineThere',
    method: 'post',
    params
  })
}

// 判断项目阶段是否导入数据
export function checkProjectStageData(params) {
  return request({
    url: '/project/detail/checkProjectStageData',
    method: 'post',
    params,
  })
}
