import request from '@/utils/request'

// 获取图形验证码
export function authImg() {
  return request({
    url: '/sys/login/authImg',
    method: 'get',
    responseType: 'arraybuffer'
  })
}

// 登录
export function login(params) {
  return request({
    // url: '/sys/login/login',
    url: '/sys/login/loginHome',
    method: 'post',
    params
  })
}

// 退出
export function logout(params) {
  return request({
    url: '/sys/login/logout',
    method: 'post',
    params
  })
}

// 获取当前用户信息
export function getCurUser() {
  return request({
    url: '/sys/user/getCurUser',
    method: 'post'
  })
}

// 获取系统信息
export function getSysInfo() {
  return request({
    url: '/sys/getSysInfo',
    method: 'post'
  })
}

// 配置系统名称
export function configSysName(params) {
  return request({
    url: '/sys/configSysName',
    method: 'post',
    params
  })
}

// 获取账号列表
export function getUserPageList(data) {
  return request({
    url: '/sys/getUserPageList',
    method: 'post',
    data
  })
}

// 新增或更新账号
export function addOrUpdateUser(data) {
  return request({
    url: '/sys/addOrUpdateUser',
    method: 'post',
    data
  })
}

// 重置账号密码
export function resetPwd(params) {
  return request({
    url: '/sys/resetPwd',
    method: 'post',
    params
  })
}

// 更换角色
export function updateRole(params) {
  return request({
    url: '/sys/updateRole',
    method: 'post',
    params
  })
}

// 删除账号
export function delUser(params) {
  return request({
    url: '/sys/delUser',
    method: 'post',
    params
  })
}

// 获取角色列表
export function getRoleList(data) {
  return request({
    url: '/sys/getRoleList',
    method: 'post',
    data
  })
}

// 删除角色
export function delRole(params) {
  return request({
    url: '/sys/delRole',
    method: 'post',
    params
  })
}

// 修改账号信息（个人信息）
export function updateInfo(data) {
  return request({
    url: '/sys/user/updateInfo',
    method: 'post',
    data
  })
}

// 修改密码
export function updatePwd(params) {
  return request({
    url: '/sys/user/updatePwd',
    method: 'post',
    params
  })
}

// 上传用户头像
export function uploadUserImg(data) {
  return request({
    url: '/sys/user/uploadUserImg',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 查询菜单树状列表
export function getMenuTree() {
  return request({
    url: '/sys/getMenuTree',
    method: 'post'
  })
}

// 新增或更新角色
export function addOrUpdateRole(data) {
  return request({
    url: '/sys/addOrUpdateRole',
    method: 'post',
    data
  })
}

// 启用/禁用角色接口
export function updateRoleStatus(data) {
  return request({
    url: '/sys/updateRoleStatus',
    method: 'post',
    data
  })
}

// 获取角色详细信息
export function getRoleInfo(params) {
  return request({
    url: '/sys/getRoleInfo',
    method: 'post',
    params
  })
}

// 解除登录锁定
export function unlockLogin(data) {
  return request({
    url: '/sys/login/unlockLogin',
    method: 'post',
    data
  })
}
// 配置系统LOGO
export function configSysLogo(data) {
  return request({
    url: '/sys/configSysLogo',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

