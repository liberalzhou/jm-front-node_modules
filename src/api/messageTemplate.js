import request from '@/utils/request'

// 获取模板列表
export function getTemplateListPage(data) {
  return request({
    url: '/sys/template/listPage',
    method: 'POST',
    data
  })
}

// 获取邮箱列表
export function getEmailPageList(data) {
  return request({
    url: '/sys/email/getEmailPageList',
    method: 'POST',
    data
  })
}

// 新增或更新模板
export function addOrUpdateInformTemplate(data) {
  return request({
    url: '/sys/template/addOrUpdateInform',
    method: 'POST',
    data
  })
}

// 删除模板
export function delInformTemplate(params) {
  return request({
    url: '/sys/template/delMessageTemplate',
    method: 'POST',
    params
  })
}

// 新增或更新Email
export function addOrUpdateEmail(data) {
  return request({
    url: '/sys/email/addOrUpdateEmail',
    method: 'POST',
    data
  })
}

// 获取邮箱列表
export function getEmailList(params) {
  return request({
    url: '/sys/email/getEmailListByUserId',
    method: 'POST',
    params
  })
}

// 删除邮箱
export function delEmail(params) {
  return request({
    url: '/sys/email/deleteEmail',
    method: 'POST',
    params
  })
}

