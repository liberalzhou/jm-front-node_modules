import request from '@/utils/request'

// 获取材价分类(自定义分类、国际分类)
export function getCjType() {
  return request({
    url: '/mpcmaterail/matprice/getMaterialSort',
    method: 'post'
  })
}

// 获取类型列表
export function getSysDictList(params) {
  return request({
    url: '/sys/common/getSysDictList',
    method: 'post',
    params
  })
}

// 获取类型信息
export function getSysDict(params) {
  return request({
    url: '/sys/common/getSysDict',
    method: 'post',
    params
  })
}

// 材价汇总列表
export function queryMaterialCount(data) {
  return request({
    // url: '/mpccollect/materialpricecount/queryMaterialCount',
    url: '/mpccollect/evaluate/queryMaterialCount',
    method: 'post',
    data
  })
}

// 新增评价
export function addEvaluate(data) {
  return request({
    url: '/mpccollect/evaluate/addEvaluate',
    method: 'post',
    data
  })
}

// 材价汇总列表导出
export function exportMaterialCount(data) {
  return request({
    // url: '/mpccollect/materialpricecount/exportMaterialCount',
    url: '/mpccollect/evaluate/exportMaterialCount',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 材价汇总详情列表
export function queryMaterialDetailCount(data) {
  return request({
    // url: '/mpccollect/materialpricecount/queryMaterialDetailCount',
    url: '/mpccollect/evaluate/queryMaterialDetailCount',
    method: 'post',
    data
  })
}

// 报价及时性列表
export function selestPriceSpeedSetList(data) {
  return request({
    url: '/mpccollect/evaluate/selestPriceSpeedSetList',
    method: 'post',
    data
  })
}

// 报价及时性设置
export function updateAddPriceSpeed(data) {
  return request({
    url: '/mpccollect/evaluate/updateAddPriceSpeed',
    method: 'post',
    data
  })
}

// 根据日期获取周期列表
export function getPeriodSettList(data) {
  return request({
    url: '/sys/common/getPeriodSettList',
    method: 'post',
    data
  })
}

// 材价详情列表
export function queryMaterialDetail(data) {
  return request({
    url: '/mpccollect/evaluate/queryMaterialDetail',
    method: 'post',
    data
  })
}

// 评价考核详情导出
export function exportMaterialDetailCount(data) {
  return request({
    url: '/mpccollect/evaluate/exportMaterialDetailCount',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 评分统计
export function getScoreLineDetail(params) {
  return request({
    url: '/mpccollect/evaluate/getScoreLineDetail',
    method: 'post',
    params
  })
}

// 信息均价库详情列表
export function getAvgMessagePriceDetailListPage(data) {
  return request({
    url: '/mpccollect/messageAvgPriceDetail/getAvgMessagePriceDetailListPage',
    method: 'post',
    data
  })
}

// 获取信息均价库或期刊库自定义分类
export function getCustomTypeList(params) {
  return request({
    url: '/mpccollect/messageAvgPriceDetail/getCustomTypeList',
    method: 'get',
    params
  })
}
// 获取材价分类(自定义分类、国际分类)
export function getMaterialSort(data) {
  return request({
    url: '/mpccollect/messageAvgPriceDetail/getMaterialSort',
    method: 'post',
    data
  })
}

// 导出信息均价库详情列表excel
export function exportAvgMessagePriceDetailFile(data) {
  return request({
    url: '/mpccollect/messageAvgPriceDetail/exportAvgMessagePriceDetailFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}
// 出信息均价库主页列表excel
export function exportAvgMessagePriceFile(data) {
  return request({
    url: '/mpccollect/messageAvgPriceDetail/exportAvgMessagePriceFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}
// --------------------------------------------------------------------------------------

// 获取材价列表
export function getCjData(data) {
  return request({
    url: '/mpcmaterail/matprice/getMaterialDetailPage',
    method: 'post',
    data
  })
}

// 删除材价
export function deleteCjItem(params) {
  return request({
    url: '/mpcmaterail/matprice/deleteMatPrice',
    method: 'post',
    params
  })
}

// 导出材价信息
export function exportTempFile(data) {
  return request({
    url: '/mpcmaterail/matprice/exportMaterialDetailFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 获取当前省份下的城市
export function getCityList(params) {
  return request({
    url: '/mpcmaterail/matprice/getCityList',
    method: 'post',
    params
  })
}

// 导入材价文件
export function importCjFile(data) {
  return request({
    url: '/mpcmaterail/matprice/importMatPrice',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 更新材料自定义分类
export function updateCjType(params) {
  return request({
    url: '/mpcmaterail/matprice/updateCustType',
    method: 'post',
    params
  })
}

// 更新材料自定义分类
export function joinThisPeriodTemplate(params) {
  return request({
    url: '/mpcmaterail/matprice/joinThisPeriodTemplate',
    method: 'post',
    params
  })
}

// 下载材价导入模板
export function uploadMaterialTemp(params) {
  return request({
    url: '/mpcmaterail/matprice/getMatPriceImportTemplate',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 修改材价信息
export function updateMatPrice(data) {
  return request({
    url: '/mpcmaterail/matprice/updateMatPrice',
    method: 'post',
    data
  })
}

// 根据周期和材料获取报价人列表
export function getQuoterList(params) {
  return request({
    url: '/mpcmaterail/matprice/getQuoterList',
    method: 'post',
    params
  })
}

// 材价详情列表导出
export function exportMaterialDetail(data) {
  return request({
    url: '/mpccollect/evaluate/exportMaterialDetail',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 材价详情列表全部导出
export function exportAllMaterialDetail(data) {
  return request({
    url: '/mpccollect/materialpricecount/exportAllMaterialDetail',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 采集员导入材价周期和模板
export function getOperablePeriodList() {
  return request({
    url: '/sys/common/getOperablePeriodList',
    method: 'post'
  })
}

// 重新标准化(材价)
export function againFormatPrice(params) {
  return request({
    url: '/mpcmaterail/matprice/againFormat',
    method: 'post',
    params
  })
}

// 调整国标分类
export function updateSubCId(params) {
  return request({
    url: '/mpcmaterail/matprice/updateSubCId',
    method: 'post',
    params
  })
}

// 新增或更新材价
export function addOrUpdateMatPrice(data) {
  return request({
    url: '/mpcmaterail/matprice/addOrUpdateMatPrice',
    method: 'post',
    data
  })
}

