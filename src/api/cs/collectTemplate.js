import request from '@/utils/request'
// 模板清单列表
export function getTempList(data) {
  return request({
    url: '/mpccollect/collectTemplate/getCollectTemplateListPage',
    method: 'post',
    data
  })
}

// 检查模板下是否存在材料
export function checkTemp(params) {
  return request({
    url: '/mpccollect/collectTemplate/checkTemplateIsExistDetail',
    method: 'get',
    params
  })
}

// 删除模板
export function deleteTemp(params) {
  return request({
    url: '/mpccollect/collectTemplate/batchDelTemplate',
    method: 'post',
    params
  })
}

// 根据创建人获取周期信息
export function getPeriodSettByCreateOn(params) {
  return request({
    url: '/mpccollect/collectTemplate/getPeriodSettByCreateOn',
    method: 'get',
    params
  })
}

// 根据周期获取历史信息
export function getHistoryTemplateList(params) {
  return request({
    url: '/mpccollect/collectTemplate/getHistoryTemplateList',
    method: 'get',
    params
  })
}

// 添加备注
export function tempRemark(data) {
  return request({
    url: '/mpccollect/collectTemplateDetail/updTemplateDetail',
    method: 'post',
    data
  })
}
// // 新增/修改模板
// export function addOrUpdateTemp(data) {
//   return request({
//     url: '/mpccollect/collectTemplate/addOrUpdCollectTemplate',
//     method: 'post',
//     data
//   })
// }
// 新增模板
export function addCollectTemplate(data) {
  return request({
    url: '/mpccollect/collectTemplate/addCollectTemplate',
    method: 'post',
    data
  })
}

// 获取模板自定义分类
export function getTempCustomType(params) {
  return request({
    url: '/mpccollect/collectTemplate/getCustomTypeList',
    method: 'get',
    params
  })
}

// 根据日期获取周期列表（针对综合价库的周期）
export function getExistsTempPeriodList(params) {
  return request({
    url: '/sys/common/getExistsTempPeriodList',
    method: 'post',
    params
  })
}

// 查看反馈信息
export function getBackfeed(data) {
  return request({
    url: '/mpccollect/collectTemplate/getBackfeed',
    method: 'post',
    data
  })
}

// 上传营业执照
export function uploadBusinessLicenseImg(data) {
  return request({
    url: '/sz2sys/userExtend/uploadBusinessLicenseImg',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}
// --------------------------------------------------------------------------------

// 新增单条 自定义/非自定义 材料
export function addMaterial(data, params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/addMaterial',
    method: 'post',
    data,
    params
  })
}

// 模板材料编辑
export function editTemp(data) {
  return request({
    url: '/mpccollect/collectTemplateDetail/updateTempMaterial',
    method: 'post',
    data
  })
}

// 根据日期获取周期列表
export function getCycleInfo(params) {
  return request({
    url: '/sys/common/getPeriodSettList',
    method: 'post',
    params
  })
}

// 模板清单详情列表
export function getTempDetailList(data) {
  return request({
    url: '/mpccollect/collectTemplateDetail/getCollectTemplateDetailListPage',
    method: 'post',
    data
  })
}

// 删除模板详情材料
export function deleteItem(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/deleteTemplateDetail',
    method: 'post',
    params
  })
}

// 导出模板详情材料
export function exportTempDetail(data) {
  return request({
    url: '/mpccollect/collectTemplateDetail/exportMaterialDetailFile',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 修改模板详情材料序号
export function updateNum(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/updTemplateDetailSort',
    method: 'post',
    params
  })
}

// 模板详情下载模板excel
export function uploadTemp(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/downloadTemplateFile',
    method: 'post',
    params,
    responseType: 'blob'
  })
}

// 模板详情导入材料
export function importFile(data) {
  return request({
    // url: '/mpccollect/collectTemplateDetail/importMaterial',
    url: '/mpccollect/collectTemplateDetail/importStandardMaterial',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 调整分类
export function updCustomType(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/updCustomType',
    method: 'post',
    params
  })
}

// 获取最新周期下模板清单详情列表
export function getNewTemplateDetailListPage(data) {
  return request({
    url: '/mpccollect/collectTemplateDetail/getNewTemplateDetailListPage',
    method: 'post',
    data
  })
}

// 修改模板
export function updCollectTemplate(data) {
  return request({
    url: '/mpccollect/collectTemplate/updCollectTemplate',
    method: 'post',
    data
  })
}

// 重新标准化(材料)
export function againFormat(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/againFormat',
    method: 'post',
    params
  })
}

// 调整国标分类
export function updateSubCId(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/updateSubCId',
    method: 'post',
    params
  })
}

// 材料一级二级国标分类（含默认税率）
export function materialDropDownBox(params) {
  return request({
    url: '/mpccollect/collectTemplateDetail/materialDropDownBox',
    method: 'post',
    params
  })
}
