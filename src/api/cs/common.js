import request from '@/utils/request'

// 获取默认城市和省份
export function getDefaultCity() {
  return request({
    url: '/sys/common/getDefaultCity',
    method: 'post'
  })
}
// ------------------------------------------------------------------------------------------------------

// 新增或修改类型信息
export function addOrUpdateSysDict(data) {
  return request({
    url: '/sys/common/addOrUpdateSysDict',
    method: 'post',
    data
  })
}

// 删除类型
export function delSysDictInfo(params) {
  return request({
    url: '/sys/common/delSysDictInfo',
    method: 'post',
    params
  })
}

// 查询第三方接口配置
export function getThirdPartyInterface(params) {
  return request({
    url: '/mpcmaterail/config/getThirdPartyInterface',
    method: 'post',
    params
  })
}

// 更新第三方接口配置
export function updatedThirdPartyInterface(params) {
  return request({
    url: '/mpcmaterail/config/updateThirdPartyInterface',
    method: 'post',
    params
  })
}

// 获取自定义分类
export function getCustomizeSort(params) {
  return request({
    url: '/mpcmaterail/config/getCustomizeSort',
    method: 'post',
    params
  })
}

// 新增或更新自定义分类
export function addOrUpdateCustomizeSort(data) {
  return request({
    url: '/mpcmaterail/config/addOrUpdateCustomizeSort',
    method: 'post',
    data
  })
}

// 删除材价自定义分类
export function delCustomizeSort(params) {
  return request({
    url: '/mpcmaterail/config/delCustomClassify',
    method: 'post',
    params
  })
}

// 获取模板自定义分类
export function getCustomTypeList(params) {
  return request({
    url: '/mpccollect/collectTemplate/getCustomTypeList',
    method: 'get',
    params
  })
}

// 新增/修改/删除模板自定义分类
export function changeCustomType(data) {
  return request({
    url: '/mpccollect/collectTemplate/changeCustomType',
    method: 'post',
    data
  })
}

// 下载失败清单文件
export function downloadErrorExcel(params) {
  return request({
    url: '/sys/common/downloadErrorExcel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

// 删除模板自定义分类
export function delCustomType(data) {
  return request({
    url: '/mpccollect/collectTemplate/delCustomType',
    method: 'post',
    data
  })
}

// 新增模板自定义分类
export function addCustomType(data) {
  return request({
    url: '/mpccollect/collectTemplate/addCustomType',
    method: 'post',
    data
  })
}

// 修改模板自定义分类
export function updCustomType(data) {
  return request({
    url: '/mpccollect/collectTemplate/updCustomType',
    method: 'post',
    data
  })
}

// 获取一级国标分类列表
export function getSubcSort() {
  return request({
    url: '/sys/common/getSubcSort',
    method: 'post'
  })
}

// 自定义分类绑定国标
export function configSort(params) {
  return request({
    url: '/mpcmaterail/config/typeAssociate',
    method: 'post',
    params
  })
}
