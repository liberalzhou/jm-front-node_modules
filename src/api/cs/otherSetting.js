import request from '@/utils/request'

// 获取城市的综合价
export function getCityMats() {
    return request({
      url: '/order/config/ZJT/get_city_mats',
      method: 'get'
    })
}

// 综合价导入
export function comPriceExport(data) {
    return request({
      url: '/order/config/comPriceExport',
      method: 'post',
      data,
      headers: {
        'Content-Type': 'multipart/form-data',
      }
    })
}

// 获取综合价时间
export function getComPriceTime() {
    return request({
      url: '/order/config/getComPriceTime',
      method: 'get'
    })
}

// 更新综合价时间
export function updateComPriceTime(params) {
    return request({
      url: '/order/config/updateComPriceTime',
      method: 'post',
      params
    })
}

// 获取发布状态
export function getIssuedStatus() {
  return request({
    url: '/order/config/getIssuedStatus',
    method: 'get'
  })
}

// 更新发布状态
export function updateIssued(params) {
    return request({
      url: '/order/config/updateIssued',
      method: 'post',
      params
    })
}

// 导出综合价材料清单下载
export function downloadMatTemplate() {
    return request({
      url: '/order/config/downloadMatTemplate',
      method: 'get',
      responseType: 'blob'
    })
}

