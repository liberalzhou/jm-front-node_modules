import request from '@/utils/request'

// 查询综合价列表
export function getComplexPriceList(data) {
  return request({
    url: '/complex/getComplexPriceList',
    method: 'post',
    data
  })
}

// 修改综合价材料序号
export function updTemplateDetailSort(params) {
  return request({
    url: '/complex/updTemplateDetailSort',
    method: 'post',
    params
  })
}

// 获取测算状态情况
export function getCalInfo(params) {
  return request({
    url: '/complex/getCalInfo',
    method: 'post',
    params
  })
}

// 获取报价情况
export function getQuoteInfo(params) {
  return request({
    url: '/complex/getQuoteInfo',
    method: 'post',
    params
  })
}

// 价格测算
export function priceCal(data) {
  return request({
    url: '/complex/matPriceCal',
    method: 'post',
    data
  })
}

// 预测算
export function prepPriceCal(data) {
  return request({
    url: '/complex/prepPriceCal',
    method: 'post',
    data
  })
}

// 保存为新测算
export function newMatPriceCal(data) {
  return request({
    url: '/complex/newMatPriceCal',
    method: 'post',
    data
  })
}

// 导出综合价列表
export function exportComplexPriceList(data) {
  return request({
    url: '/complex/exportComplexPriceList',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 发布编制价格
export function releaseCompPrice(data) {
  return request({
    url: '/complex/releaseCompPrice',
    method: 'post',
    data
  })
}

// 查询材料测算列表
export function getCalRecordList(data) {
  return request({
    url: '/complex/getCalRecordList',
    method: 'post',
    data
  })
}

// 导出材料测算记录列表
export function exportCalRecordList(data) {
  return request({
    url: '/complex/exportCalRecordList',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 导出材料测算记录列表
export function exportCompPriceList(data) {
  return request({
    url: '/complex/exportCompPriceList',
    method: 'post',
    data,
    responseType: 'blob'
  })
}


// 查询编制价格列表
export function getCompPriceList(data) {
  return request({
    url: '/complex/getCompPriceList',
    method: 'post',
    data
  })
}

// 查询材料测算记录详细
export function getCalRecordDetail(params) {
  return request({
    url: '/complex/getCalRecordDetail',
    method: 'post',
    params
  })
}

// 获取上期材料来源平均报价
export function getSourceAveragePrice(params) {
  return request({
    url: '/complex/getSourceAveragePrice',
    method: 'post',
    params
  })
}

// 获取历史信息价走势
export function getHistoryPriceLineChart(params) {
  return request({
    url: '/complex/getHistoryPriceLineChart',
    method: 'post',
    params
  })
}

// 获取采集员报价情况
export function getUserMatPriceList(params) {
  return request({
    url: '/complex/getUserMatPriceList',
    method: 'post',
    params
  })
}

// 综合价评审清单列表
export function getCompReviewListPage(data) {
  return request({
    url: '/mpccollect/compReview/getCompReviewListPage',
    method: 'post',
    data
  })
}

// 获取评审记录列表
export function getCompReviewRecordList(params) {
  return request({
    url: '/mpccollect/compReview/getCompReviewRecordList',
    method: 'get',
    params
  })
}

// 综合价评审接口
export function compReview(data) {
  return request({
    url: '/mpccollect/compReview/compReview',
    method: 'post',
    data
  })
}

// 获取评审意见列表
export function getCompReviewOpinionList(params) {
  return request({
    url: '/mpccollect/compReview/getCompReviewOpinionList',
    method: 'get',
    params
  })
}

// 获取周期综合价信息
export function getTempDetailInfo(params) {
  return request({
    url: '/complex/getTempDetailInfo',
    method: 'post',
    params
  })
}

// 提交审核校验
export function checkSubmitAudit(params) {
  return request({
    url: '/complex/validateSubmitAudit',
    method: 'post',
    params
  })
}

// 提交审核
export function submitAudit(params) {
  return request({
    url: '/complex/submitAudit',
    method: 'post',
    params
  })
}

// 根据ID获取综合价评审详情
export function getCompReviewById(params) {
  return request({
    url: '/mpccollect/compReview/getCompReviewById',
    method: 'get',
    params
  })
}

// 添加评审意见
export function addCompReviewOpinion(params) {
  return request({
    url: '/mpccollect/compReview/addCompReviewOpinion',
    method: 'post',
    params
  })
}

// 查询本次驳回意见
export function getThisTimeOpinion(params) {
  return request({
    url: '/mpccollect/compReview/getThisTimeOpinion',
    method: 'post',
    params
  })
}

// 添加或更新驳回意见
export function addOrUpdateOpinion(params) {
  return request({
    url: '/mpccollect/compReview/addOrUpdateOpinion',
    method: 'post',
    params
  })
}

// 删除本次驳回意见
export function deleteOpinion(params) {
  return request({
    url: '/mpccollect/compReview/deleteOpinion',
    method: 'post',
    params
  })
}

// 校验是否有驳回
export function validateRejectCount(params) {
  return request({
    url: '/mpccollect/compReview/validateRejectCount',
    method: 'post',
    params
  })
}

// 调整序号
export function operateSortNo(params) {
  return request({
    url: '/complex/operateSortNo',
    method: 'post',
    params
  })
}

// 获取周期模板税率信息
export function getTaxPointInfo(params) {
  return request({
    url: '/complex/getTaxPointInfo',
    method: 'post',
    params
  })
}

// 保存周期模板税率信息
export function addTaxPointInfo(data) {
  return request({
    url: '/complex/addTaxPointInfo',
    method: 'post',
    data
  })
}

// 保存周期模板税点信息(无国标分类)
export function addTaxPointInfoNoSubcId(data) {
  return request({
    url: '/complex/addTaxPointInfoNoSubcId',
    method: 'post',
    data
  })
}

// 查询待编制的材料列表
export function getWaitCompList(params) {
  return request({
    url: '/complex/getWaitCompList',
    method: 'post',
    params
  })
}

// 导出综合价评审列表
export function exportAuditList(data) {
  return request({
    url: '/complex/exportAuditList',
    method: 'post',
    data,
    responseType: 'blob'
  })
}

// 查询无国标的材料
export function checkNoSubcIdMat(params) {
  return request({
    url: '/complex/checkNoSubcIdMat',
    method: 'post',
    params
  })
}
