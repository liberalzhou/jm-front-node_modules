import request from '@/utils/request'

// 同类工程 - 获取项目换算工程列表
export function getMatrixProjectPage(data) {
    return request({
        url: '/matrix/project/getMatrixProjectPage',
        method: 'post',
        data
    })
}

// 同类工程 - 获取单项、单位工程
export function getMatrixProjectUnit(params) {
    return request({
        url: '/matrix/project/getMatrixProjectUnit',
        method: 'post',
        params
    })
}

// 同类工程 - 获取换算后的单项工程合计
export function matrixTotalPrice(data) {
    return request({
        url: '/matrix/project/matrixTotalPrice',
        method: 'post',
        data
    })
}

// 同类工程 - 材价换算列表
export function listMatrixMatPage(data) {
    return request({
        url: '/matrix/project/listMatrixMatPage',
        method: 'post',
        data
    })
}

// 同类工程 - 更多价格
export function getMoreOtherMatPrice(data) {
    return request({
        url: '/matrix/project/getMoreOtherMatPrice',
        method: 'post',
        data
    })
}

// 同类工程 - 保存方案
export function saveProjectMatrix(data) {
    return request({
        url: '/matrix/project/saveProjectMatrix',
        method: 'post',
        data
    })
}





// 单项工程 - 获取单项工程列表
export function gainListMatrixSinglePage(data) {
    return request({
        url: '/project/matrix/single/gainListMatrixSinglePage',
        method: 'post',
        data
    })
}

// 单项工程 - 获取单位工程
export function listMatrixUnitInfo(params) {
    return request({
        url: '/project/matrix/single/listMatrixUnitInfo',
        method: 'post',
        params
    })
}

// 单项工程 - 获取换算后的单项工程合计
export function matrixSingleTotalPrice(data) {
    return request({
        url: '/project/matrix/single/matrixSingleTotalPrice',
        method: 'post',
        data
    })
}

// 单项工程 - 材价换算列表
export function listMatrixSinglePage(data) {
    return request({
        url: '/project/matrix/single/listMatrixSinglePage',
        method: 'post',
        data
    })
}

// 单项工程 - 更多价格
export function moreSinglePrice(data) {
    return request({
        url: '/project/matrix/single/moreSinglePrice',
        method: 'post',
        data
    })
}

// 单项工程 - 保存方案
export function saveSingleProjectMatrix(data) {
    return request({
        url: '/project/matrix/single/saveProjectMatrix',
        method: 'post',
        data
    })
}





// 同类工程历史 - 换算历史记录列表
export function getMatrixProjectHistoryPage(data) {
    return request({
        url: '/matrix/project/history/getMatrixProjectHistoryPage',
        method: 'post',
        data
    })
}

// 同类工程历史 - 导出换算历史记录表
export function exportMatrixProject(data) {
    return request({
        url: '/matrix/project/history/exportMatrixProject',
        method: 'post',
        data,
        responseType: 'blob'
    })
}

// 同类工程历史 - 批量删除换算历史记录
export function delMatrixProjectHistory(params) {
    return request({
        url: '/matrix/project/history/delMatrixProjectHistory',
        method: 'post',
        params
    })
}

// 同类工程历史 - 获取换算的单项工程以及单位数据
export function getMatrixHistoryInfo(params) {
    return request({
        url: '/matrix/project/history/getMatrixHistoryInfo',
        method: 'post',
        params
    })
}

// 同类工程历史 - 换算历史详细信息
export function getMatrixProjectInfo(params) {
    return request({
        url: '/matrix/project/history/getMatrixProjectInfo',
        method: 'post',
        params
    })
}

// 换算历史更多价格
export function moreHistoryPrice(data) {
    return request({
        url: '/matrix/history/morePrice',
        method: 'post',
        data
    })
}

// 同类工程历史 - 新增同类工程换算
export function saveAlikeMatrixProject(data) {
    return request({
        url: '/matrix/project/history/saveAlikeMatrixProject',
        method: 'post',
        data
    })
}

// 同类工程历史 - 获取同类工程换算
export function getAlikeMatrixProjectHistory(params) {
    return request({
        url: '/matrix/project/history/getAlikeMatrixProjectHistory',
        method: 'post',
        params
    })
}

// 同类工程历史 - 获取换算的材价列表
export function getMatrixMatPricePage(data) {
    return request({
        url: '/matrix/project/history/getMatrixMatPricePage',
        method: 'post',
        data
    })
}