import request from '@/utils/request'

// 预警设置 - 获取单方造价预警数据列表
export function listPage(data) {
    return request({
        url: '/indicator/econwarn/listPage',
        method: 'post',
        data
    })
}

// 预警设置 - 更改单方造价预警状态
export function updateStatus(params) {
    return request({
        url: '/indicator/econwarn/updateStatus',
        method: 'post',
        params
    })
}

// 预警设置 - 添加单方造价预警数据
export function addIndicator(data) {
    return request({
        url: '/indicator/econwarn/save',
        method: 'post',
        data
    })
}

// 预警设置 - 修改单方造价预警数据
export function updateIndicator(data) {
    return request({
        url: '/indicator/econwarn/update',
        method: 'post',
        data
    })
}

// 预警设置 - 删除单方造价预警数据
export function deleteIndicator(params) {
    return request({
        url: '/indicator/econwarn/delete',
        method: 'post',
        params
    })
}

// 预警设置 - 查看单方造价预警数据 
export function lookIndicator(params) {
    return request({
        url: '/indicator/econwarn/info',
        method: 'post',
        params
    })
}

// 预警设置(自定义预警) - 获取单位工程
export function getCustomProject(params) {
    return request({
        url: '/indicator/econscopewarn/getCustomProject',
        method: 'post',
        params
    })
}

// 预警设置(自定义预警) - 查看单方造价自定义预警详细数据
export function getCustomInfo(params) {
    return request({
        url: '/indicator/econscopewarn/info',
        method: 'post',
        params
    })
}

// 预警设置(自定义预警) - 添加或编辑单方造价自定义预警数据
export function editCustomInfo(data) {
    return request({
        url: '/indicator/econscopewarn/saveOrUpdate',
        method: 'post',
        data
    })
}

// 预警设置 - 地区效验
export function verifyCity(data) {
    return request({
        url: '/indicator/econwarn/verifyCity',
        method: 'post',
        data
    })
}

// 获取字典  
export function dictionary(params) {
    return request({
        url: '/indicator/dict/list',
        method: 'post',
        params
    })
}

// 获取匹配的技术特征  
export function getTecFeature(params) {
    return request({
        url: '/project/tecfeature/config/getTecFeature',
        method: 'post',
        params
    })
}

// 常用综合指标配置 - 获取配置列表
export function pluralListPage(data) {
    return request({
        url: '/indicator/plural/config/listPage',
        method: 'post',
        data
    })
}

// 常用综合指标配置 - 更改常用综合指标配置状态
export function pluralUpdateStatus(params) {
    return request({
        url: '/indicator/plural/config/updateStatus',
        method: 'post',
        params
    })
}

// 常用综合指标配置 - 地区校验
export function pluralVerifyCity(data) {
    return request({
        url: '/indicator/plural/config/verifyCity',
        method: 'post',
        data
    })
}

// 常用综合指标配置 - 添加单方造价预警数据
export function pluralAddIndicator(data) {
    return request({
        url: '/indicator/plural/config/save',
        method: 'post',
        data
    })
}

// 常用综合指标配置 - 修改单方造价预警数据
export function pluralUpdateIndicator(data) {
    return request({
        url: '/indicator/plural/config/update',
        method: 'post',
        data
    })
}

// 常用综合指标配置 - 查看单方造价预警数据 
export function pluralLookIndicator(params) {
    return request({
        url: '/indicator/plural/config/info',
        method: 'post',
        params
    })
}

// 常用综合指标配置 - 删除单方造价预警数据
export function pluralDeleteIndicator(params) {
    return request({
        url: '/indicator/plural/config/delete',
        method: 'post',
        params
    })
}
