# 建设工程造价大数据管理应用平台

## Build Setup

浏览器访问 [http://localhost:8080](http://localhost:8080)

```bash

# 安装依赖
npm install

# 启动服务
npm run dev
```

## 发布

```bash
# 項目打包
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```
