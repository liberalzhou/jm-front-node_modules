'use strict'
const path = require('path')
const defaultSettings = require('./src/settings.js')

function resolve(dir) {
  return path.join(__dirname, dir)
}

const name = defaultSettings.title || '建设工程造价大数据管理应用平台' // page title

const port = process.env.port || process.env.npm_config_port || 8080 // dev port

module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: false, // 关闭eslint方法
  // lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false, // 关闭生产环境下的SourceMap映射文件
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      '/zx-web': { // czhui
        // target: 'http://172.16.101.101:8072', // 江门测试环境101.101
        // target: 'http://172.16.4.3:8072', // 江门测试环境4.3
        // target: 'http://19.121.250.124:8072', // 客户
        // target: 'http://10.160.32.26:8106', // 江门正式 客户
        target: 'http://172.16.10.118:8072', // 周志鹏本地  江门
        // target: ' http://172.16.10.230:8072', // 蔡杨潇本地
        // target: 'http://172.16.10.137:8072', // 罗富强本地
        // target: 'http://172.16.4.3:8072', //   江门测试环境4.3
        // target: 'http://10.160.160.18:8072',
        // target: 'http://172.23.160.18:8072',
        changeOrigin: true, // 是否跨域
        pathRewrite: {
          '^/zx-web': ''
        }
      }
    }

    // before: require('./mock/mock-server.js')
  },
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  chainWebpack(config) {
    config.plugin('preload').tap(() => [
      {
        rel: 'preload',
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }
    ])

    config.plugins.delete('prefetch')

    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    config
      .when(process.env.NODE_ENV !== 'development',
        config => {
          config
            .plugin('ScriptExtHtmlWebpackPlugin')
            .after('html')
            .use('script-ext-html-webpack-plugin', [{
              inline: /runtime\..*\.js$/
            }])
            .end()
          config
            .optimization.splitChunks({
              chunks: 'all',
              cacheGroups: {
                libs: {
                  name: 'chunk-libs',
                  test: /[\\/]node_modules[\\/]/,
                  priority: 10,
                  chunks: 'initial'
                },
                elementUI: {
                  name: 'chunk-elementUI',
                  priority: 20,
                  test: /[\\/]node_modules[\\/]_?element-ui(.*)/
                },
                commons: {
                  name: 'chunk-commons',
                  test: resolve('src/components'),
                  minChunks: 3,
                  priority: 5,
                  reuseExistingChunk: true
                }
              }
            })
          config.optimization.runtimeChunk('single')
        }
      )
  }
}
